import pytest
from mollie.api.objects.payment import Payment

from app.enums import ProgrammeType
from app.models.education import Education
from app.models.mollie import Transaction, TransactionMembership
from app.models.setting_model import Setting
from app.models.user import User
from app.repository import user_repository
from app.service import mollie_service
from conftest import CustomClient


@pytest.fixture(autouse=True)
def mollie_api_client_key():
    mollie_service._mollie_client.set_api_key("test_MOLLIE_CONFTEST_KEY")


@pytest.fixture
def education_informatica(db_session):
    # noinspection PyArgumentList
    edu = Education(
        nl_name="Informatica",
        en_name="Informatica",
        programme_type=ProgrammeType.BACHELOR,
        is_via_programme=True,
        datanose_code="BSc IN",
    )
    db_session.add(edu)
    db_session.commit()
    return edu


@pytest.fixture
def registered_user_bachelor(user_factory, education_informatica, db_session):
    # A registered user, with confirmed student id, bachelor student,
    # but not yet a member.
    user: User = user_factory()
    user.password = None
    user.student_id_confirmed = True
    user.paid_date = None
    user_repository.set_user_education(db_session, user, [education_informatica])
    db_session.commit()
    return user


MOLLIE_PAYMENT_PAID = Payment(
    {
        "resource": "payment",
        "id": "tr_123456789",
        "mode": "test",
        "createdAt": "2021-05-21T20:28:47+02:00",
        "amount": {"value": "10.00", "currency": "EUR"},
        "description": "Pay via",
        "method": "ideal",
        "metadata": None,
        "status": "paid",
        "paidAt": "2021-05-21T20:28:47+02:00",
        "amountRefunded": {"value": "0.00", "currency": "EUR"},
        "amountRemaining": {"value": "35.00", "currency": "EUR"},
        "locale": "nl_NL",
        "countryCode": "NL",
        "profileId": "pfl_aaaaaaaaaa",
        "settlementId": "stl_aaaaaaaaaa",
        "sequenceType": "oneoff",
        "redirectUrl": f"https://svia.nl/mollie/return/{'a' * 32}/",
        "webhookUrl": "https://svia.nl/mollie/webhook/",
        "settlementAmount": {"value": "10.00", "currency": "EUR"},
        "details": {
            "consumerName": "Maico Timmerman",
            "consumerAccount": "GB33BUKB20201555555555",
            "consumerBic": "ABNANL2A",
        },
        "_links": {
            "self": {
                "href": "https://api.mollie.com/v2/payments/tr_123456789",
                "type": "application/hal+json",
            },
            "settlement": {
                "href": "https://api.mollie.com/v2/settlements/stl_aaaaaaaaaa",
                "type": "application/hal+json",
            },
            "documentation": {
                "href": "https://docs.mollie.com/reference/v2/payments-api/get-payment",
                "type": "text/html",
            },
        },
    }
)


@pytest.mark.parametrize(
    "method,path,request_kwargs,status_code",
    [
        ["POST", "/mollie/webhook/", {"data": {"id": "tr_123456789"}}, 200],
        ["GET", f"/mollie/return/{'a' * 32}/", {}, 302],
    ],
    ids=["webhook", "web return"],
)
def test_registered_user_payment_callback_pending(
    registered_user_bachelor: User,
    anonymous_client: CustomClient,
    db_session,
    method,
    path,
    request_kwargs: dict,
    status_code,
    requests_mocker,
    task_list,
):
    db_session.add(
        Setting(key="COPERNICA_ENABLED", value=str(True)),
    )
    db_session.commit()
    t = Transaction.from_mollie_id_status_callback_id("tr_123456789", "open", "a" * 32)
    db_session.add(t)
    db_session.flush()

    callback = TransactionMembership.from_user(registered_user_bachelor)
    callback.transaction = t
    db_session.add(callback)

    db_session.commit()

    requests_mocker.get(
        "https://api.mollie.com/v2/payments/tr_123456789", json=MOLLIE_PAYMENT_PAID
    )

    rv = anonymous_client.open(method=method, path=path, **request_kwargs)
    assert rv.status_code == status_code, rv.headers

    db_session.refresh(registered_user_bachelor)
    assert registered_user_bachelor.has_paid
    assert len(task_list) == 1
    task = task_list[0]
    assert task["sender"] == "app.task.copernica.update_user"
    assert task["body"][2]["chain"][0].task == "app.task.copernica.send_welcome_mail"
    assert {r.url for r in requests_mocker.request_history} == {
        "https://api.mollie.com/v2/payments/tr_123456789",
    }
