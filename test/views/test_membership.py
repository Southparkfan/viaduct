from unittest import mock
from unittest.mock import patch

import pytest
from flask_login import current_user
from flask_wtf.csrf import generate_csrf

from app.enums import ProgrammeType
from app.models.education import Education
from app.models.mollie import Transaction
from app.models.user import User
from app.repository import user_repository
from app.service import membership_service, mollie_service
from app.views.viewmodel.membership import MembershipBannerViewModel
from conftest import CustomClient

mollie_service_mock = mock.MagicMock(mollie_service)
mollie_service_transaction_redirect_url = (
    "https://mollie.com/__mollie_mock_redirect_url__"
)
mollie_service_mock.create_transaction.return_value = (
    Transaction(),
    mollie_service_transaction_redirect_url,
)


@pytest.fixture
def education_informatica(db_session):
    # noinspection PyArgumentList
    edu = Education(
        nl_name="Informatica",
        en_name="Informatica",
        programme_type=ProgrammeType.BACHELOR,
        is_via_programme=True,
        datanose_code="BSc IN",
    )
    db_session.add(edu)
    db_session.commit()
    return edu


@pytest.fixture
def education_premsc_is(db_session):
    # noinspection PyArgumentList
    edu = Education(
        nl_name="Pre-master Information Studies",
        en_name="Pre-master Information Studies",
        programme_type=ProgrammeType.PRE_MASTER,
        is_via_programme=True,
        datanose_code="PreMsc IS",
    )
    db_session.add(edu)
    db_session.commit()
    return edu


@pytest.fixture
def education_ai(db_session):
    # noinspection PyArgumentList
    edu = Education(
        nl_name="Artificial Intelligence",
        en_name="Artificial Intelligence",
        programme_type=ProgrammeType.MASTER,
        is_via_programme=True,
        datanose_code="MSc AI",
    )
    db_session.add(edu)
    db_session.commit()
    return edu


@pytest.fixture
def registered_user_favourer(db_session, user_factory, password):
    user: User = user_factory()
    user.password = password["hash"]
    user.favourer = True
    db_session.commit()
    return user


@pytest.fixture
def registered_user_no_studies(user_factory, db_session, password):
    # A registered user, with confirmed student id, study-less,
    # but not yet a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.paid_date = None
    user_repository.set_user_education(db_session, user, [])
    db_session.commit()
    return user


@pytest.fixture
def registered_user_bachelor(user_factory, education_informatica, db_session, password):
    # A registered user, with confirmed student id, bachelor student,
    # but not yet a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.paid_date = None
    user_repository.set_user_education(db_session, user, [education_informatica])
    db_session.commit()
    return user


@pytest.fixture
def registered_user_premaster(
    user_factory, education_informatica, education_premsc_is, db_session, password
):
    # A registered user, with confirmed student id, premaster student,
    # but not yet a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.paid_date = None
    user_repository.set_user_education(
        db_session, user, [education_informatica, education_premsc_is]
    )
    db_session.commit()
    return user


@pytest.fixture
def registered_user_master(
    user_factory, education_informatica, education_ai, db_session, password
):
    # A registered user, with confirmed student id, master student,
    # but not yet a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.paid_date = None
    user_repository.set_user_education(
        db_session, user, [education_informatica, education_ai]
    )
    db_session.commit()
    return user


@pytest.fixture
def paid_user_bachelor(user_factory, education_informatica, db_session, password):
    # A registered user, with confirmed student id, bachelor student,
    # and a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.has_paid = True
    user_repository.set_user_education(db_session, user, [education_informatica])
    db_session.commit()
    return user


@pytest.fixture
def paid_user_premaster(
    user_factory, education_informatica, education_premsc_is, db_session, password
):
    # A registered user, with confirmed student id, bachelor student,
    # and a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.has_paid = True
    user_repository.set_user_education(
        db_session, user, [education_informatica, education_premsc_is]
    )
    db_session.commit()
    return user


@pytest.fixture
def paid_user_master(
    user_factory, education_informatica, education_ai, db_session, password
):
    # A registered user, with confirmed student id, master student,
    # and a member.
    user: User = user_factory()
    user.password = password["hash"]
    user.student_id_confirmed = True
    user.has_paid = True
    user_repository.set_user_education(
        db_session, user, [education_informatica, education_ai]
    )
    db_session.commit()
    return user


def test_anonymous_user_membership_payment(anonymous_client):
    path = "/membership/payment/"
    rv = anonymous_client.get(path, follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith("/sign-in/")


def test_anonymous_user_membership_pay_complete(anonymous_client):
    path = "/membership/complete/"
    rv = anonymous_client.get(path, follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith("/sign-in/")


def test_registered_user_payment_bachelor(anonymous_client, registered_user_bachelor):
    anonymous_client.login(registered_user_bachelor)

    path = "/membership/payment/"
    rv = anonymous_client.get(path, follow_redirects=False)

    assert "Pay membership fee (&#226;&#130;&#172; 20.00)" in rv.content_main


def test_registered_user_payment_premaster(
    anonymous_client, registered_user_premaster, requests_mocker
):
    requests_mocker.post(
        "https://api.copernica.com/v2/publisher/emailing", status_code=201
    )

    anonymous_client.login(registered_user_premaster)

    path = "/membership/payment/"
    rv = anonymous_client.get(path, follow_redirects=False)

    assert rv.status_code == 302
    assert rv.location.endswith("/membership/complete/")


def test_registered_user_payment_master(
    anonymous_client, registered_user_master, requests_mocker
):
    requests_mocker.post(
        "https://api.copernica.com/v2/publisher/emailing", status_code=201
    )
    anonymous_client.login(registered_user_master)

    path = "/membership/payment/"
    rv = anonymous_client.get(path, follow_redirects=False)

    assert rv.status_code == 302
    assert rv.location.endswith("/membership/complete/")


@patch.object(membership_service, "mollie_service", mollie_service_mock)
def test_registered_user_bachelor_pay(anonymous_client, registered_user_bachelor):
    anonymous_client.login(registered_user_bachelor)

    rv = anonymous_client.get("/membership/payment/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/membership/pay/", follow_redirects=False, data={"csrf_token": generate_csrf()}
    )

    assert rv.status_code == 302
    assert rv.location == mollie_service_transaction_redirect_url


@patch.object(membership_service, "mollie_service", mollie_service_mock)
def test_paid_user_bachelor_pay(anonymous_client, paid_user_bachelor):
    anonymous_client.login(paid_user_bachelor)

    rv = anonymous_client.get("/membership/payment/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/membership/pay/", follow_redirects=True, data={"csrf_token": generate_csrf()}
    )

    assert rv.status_code == 200
    assert "Pay membership fee" in str(rv.data)


@patch.object(membership_service, "mollie_service", mollie_service_mock)
def test_paid_user_premaster_pay(anonymous_client, paid_user_premaster):
    anonymous_client.login(paid_user_premaster)

    rv = anonymous_client.get("/membership/payment/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/membership/pay/", follow_redirects=True, data={"csrf_token": generate_csrf()}
    )

    assert rv.status_code == 200
    assert "Pay membership fee" not in str(rv.data)


@patch.object(membership_service, "mollie_service", mollie_service_mock)
def test_paid_user_master_pay(anonymous_client, paid_user_master):
    anonymous_client.login(paid_user_master)

    rv = anonymous_client.get("/membership/payment/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/membership/pay/", follow_redirects=True, data={"csrf_token": generate_csrf()}
    )

    assert rv.status_code == 200
    assert "Pay membership fee" not in str(rv.data)


@pytest.mark.parametrize(
    "url",
    [
        "/membership/complete/normal/",
        "/membership/complete/free/",
        "/membership/payment/cannot/",
    ],
)
def test_special_urls(url, db_session, admin_client, admin_user, page_revision):
    page, revision = page_revision

    page.path = url[1:]
    db_session.add(page)
    db_session.commit()

    admin_client.login(admin_user)
    rv = admin_client.get(url)
    assert rv.status_code == 404


def test_favourer_no_show_banner(
    anonymous_client: CustomClient, registered_user_favourer: User
):
    anonymous_client.login(registered_user_favourer)

    # Use contextmanager to keep the request context around for assertions.
    with anonymous_client as client:
        rv = client.get("/")
        assert rv.status_code == 200
        assert current_user == registered_user_favourer
        assert MembershipBannerViewModel().membership_confirmation_pending is False


def test_registered_user_show_banner(
    anonymous_client: CustomClient, registered_user_no_studies: User
):
    anonymous_client.login(registered_user_no_studies)

    # Use contextmanager to keep the request context around for assertions.
    with anonymous_client as client:
        rv = client.get("/")
        assert rv.status_code == 200
        assert current_user == registered_user_no_studies
        assert MembershipBannerViewModel().membership_confirmation_pending is True
