import pytest

from app.models.user import User
from conftest import CustomClient

urls = [
    "/challenge/",
    "/challenge/dashboard/",
    "/admin/company/",
    "/courses/",
    "/courses/create",
    "/education",
    "/examination",
    "/groups/create",
    "/pages/",
    "/tutoring",
    "/tutoring/request",
    "/jobs/",
    "/companies/",
    "/photos/",
    "/photos/123456789",
    "/search/",
    "/pimpy/",
    "/pimpy/tasks/",
    "/pimpy/tasks/create/",
    "/pimpy/minutes/",
    "/pimpy/minutes/create/",
    "/redirect/",
    "/bug/report/",
    "/lectures/",
]


@pytest.mark.parametrize("url", urls)
def test_vue_pages_without_requirements(url, anonymous_client, admin_user):
    anonymous_client.login(admin_user)
    rv = anonymous_client.get(url)
    assert rv.status_code == 200


membership_urls = ["/photos/", "/photos/123456789"]


@pytest.mark.parametrize("membership_url", membership_urls)
def test_membership_required_pages_anonymous(
    membership_url: str, anonymous_client: CustomClient
) -> None:
    rv = anonymous_client.get(membership_url, follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.endswith("/sign-in/")


@pytest.mark.parametrize("membership_url", membership_urls)
def test_membership_required_pages_unpaid(
    membership_url: str, anonymous_client: CustomClient, unpaid_user: User
) -> None:
    anonymous_client.login(unpaid_user)
    rv = anonymous_client.get(membership_url, follow_redirects=False)
    assert rv.status_code == 403
