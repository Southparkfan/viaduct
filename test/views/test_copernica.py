import pytest

from app.models.mailinglist_model import MailingList
from app.models.setting_model import Setting
from app.models.user import User


@pytest.fixture
def mailinglists(db_session):
    mailinglist1 = MailingList(
        id=1,
        nl_name="Nieuwsbrief",
        en_name="Newsletter",
        copernica_column_name="Ingeschreven",
        members_only=True,
    )

    mailinglist2 = MailingList(
        id=2,
        nl_name="Bedrijfsinformatie",
        en_name="Company information",
        copernica_column_name="Bedrijfsinformatie",
        members_only=True,
    )

    db_session.add(mailinglist1)
    db_session.add(mailinglist2)
    db_session.commit()

    return [mailinglist1, mailinglist2]


@pytest.fixture
def user_newsletter(db_session, mailinglists, user_factory):
    user: User = user_factory()
    user.copernica_id = 12345
    user.mailinglists = [mailinglists[0]]

    db_session.commit()

    return user


def test_retrieve_webhook_verification_code(app, db_session, anonymous_client):
    filename = "copernica-123-456.txt"
    content = "1234567890qwertyuiop"

    db_session.add_all(
        [
            Setting(key="COPERNICA_WEBHOOK_VERIFICATION_ENABLED", value=str(True)),
            Setting(key="COPERNICA_WEBHOOK_VERIFICATION_FILENAME", value=filename),
            Setting(key="COPERNICA_WEBHOOK_VERIFICATION_CONTENT", value=content),
        ]
    )
    db_session.commit()

    rv = anonymous_client.get("/" + filename)

    assert rv.status_code == 200
    assert rv.data.decode("utf-8") == content


def test_retrieve_webhook_verification_code_disabled(db_session, anonymous_client):
    filename = "copernica-123-456.txt"

    db_session.add_all(
        [
            Setting(key="COPERNICA_WEBHOOK_VERIFICATION_ENABLED", value=False),
            Setting(key="COPERNICA_WEBHOOK_VERIFICATION_FILENAME", value=filename),
            Setting(key="COPERNICA_WEBHOOK_VERIFICATION_CONTENT", value=""),
        ]
    )
    db_session.commit()
    rv = anonymous_client.get("/" + filename)

    assert rv.status_code == 404


def test_call_webhook_correct_token(
    db_session, anonymous_client, user_newsletter, mailinglists, task_list
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "database": "1",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200
    assert (
        user_newsletter.mailinglists.count() == 1
        and user_newsletter.mailinglists[0] == mailinglists[1]
    )
    assert task_list == [], "webhook should not trigger Copernica sync task"


def test_call_webhook_incorrect_token(
    db_session, anonymous_client, user_newsletter, mailinglists
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value="True"),
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "database": "1",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=somethingelse", data=webhook_data
    )

    assert rv.status_code == 200

    # Assert that the user's preferences have not changed
    assert (
        user_newsletter.mailinglists.count() == 1
        and user_newsletter.mailinglists[0] == mailinglists[0]
    )


def test_call_webhook_incorrent_type(
    db_session, anonymous_client, user_newsletter, mailinglists
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value="True"),
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "create",
        "profile": "54321",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_incorrent_database(
    db_session, anonymous_client, user_newsletter, mailinglists
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value="True"),
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "database": "321",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_incorrent_profile(
    db_session, anonymous_client, user_newsletter, mailinglists
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value="True"),
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "update",
        "profile": "54321",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "parameters[NogEenMailinglijst]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_non_mailinglist_parameters(
    db_session, anonymous_client, user_newsletter, mailinglists
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value="True"),
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "parameters[Bedrijfsinformatie]": "Ja",
        "parameters[NogEenOnbekendeMailinglijst]": "Ja",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200


def test_call_webhook_missing_mailinglist_parameters(
    db_session, anonymous_client, user_newsletter, mailinglists
):
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value="True"),
            Setting(key="COPERNICA_DATABASE_ID", value="1"),
            Setting(key="COPERNICA_WEBHOOK_TOKEN", value="123abc"),
        ]
    )
    db_session.commit()

    webhook_data = {
        "type": "update",
        "profile": "12345",
        "database": "1",
        "parameters[Ingeschreven]": "Nee",
        "timestamp": "1979-02-12 12:49:23",
        "created": "1979-02-12 12:49:23",
        "modified": "1979-02-12 12:49:23",
    }

    rv = anonymous_client.post(
        "/copernica/webhook/?auth_token=123abc", data=webhook_data
    )

    assert rv.status_code == 200
