from datetime import date

import pytest
from flask_wtf.csrf import generate_csrf
from sqlalchemy.orm import Session

from app.models.alv_model import Alv


@pytest.fixture
def some_alv(db_session: Session, admin_user) -> Alv:
    a = Alv()
    a.en_name = a.nl_name = "Some ALV"
    a.secretary_user_id = admin_user.id
    a.chairman_user_id = admin_user.id
    a.date = date.today()
    db_session.add(a)
    db_session.commit()
    return a


def test_create_alv(admin_user, admin_client, activity_factory):
    admin_client.login(admin_user)

    activity = activity_factory()

    rv = admin_client.get("/alv/create/")
    assert rv.status_code == 200

    rv = admin_client.post(
        "/alv/create/",
        data={
            "csrf_token": generate_csrf(),
            "nl_name": "nl_name",
            "en_name": "en_name",
            "date": "2019-11-16",
            "activity": activity.id,
            "chairman": admin_user.id,
            "secretary": admin_user.id,
        },
    )
    assert rv.status_code == 200


def test_latest_alv(some_alv, admin_client, admin_user):
    admin_client.login(admin_user)
    rv = admin_client.get("/alv/latest/", follow_redirects=False)
    assert rv.location.endswith(f"/alv/{some_alv.id}/")
