from app.service import redirect_service
from app.task import redirect


def test_redirect_removal_past(db, admin_client, admin_user):
    # Redirect in the past (ended in 2000)
    activity_request = admin_client.post(
        "/api/activities/",
        json={
            "name": {"en": "asdf", "nl": "asdf"},
            "description": {"en": "adsf", "nl": "asdf"},
            "price": "1",
            "location": "Studievereniging VIA, Science Park 904, Amsterdam",
            "start_time": "2000-01-01T13:35:51.436Z",
            "end_time": "2000-02-02T13:35:51.436Z",
            "pretix_event_slug": "",
        },
    )

    assert activity_request.status_code == 200, activity_request.json

    activity_id = activity_request.json["id"]
    redirect_service.create_redirection(
        "/test", f"http://svia.nl/activities/{activity_id}"
    )

    assert (
        redirect_service.find_by_path("/test").to
        == f"http://svia.nl/activities/{activity_id}"
    )

    redirect.remove_expired_redirects()

    assert redirect_service.find_by_path("/test") is None


def test_redirect_removal_future(db, admin_client, admin_user):
    # Redirect in the future (ends in 9999)
    activity_request = admin_client.post(
        "/api/activities/",
        json={
            "name": {"en": "asdfg", "nl": "asdfg"},
            "description": {"en": "adsfg", "nl": "asdfg"},
            "price": "2",
            "location": "Studievereniging VIA, Science Park 904, Amsterdam",
            "start_time": "9999-01-01T13:35:51.436Z",
            "end_time": "9999-02-02T13:35:51.436Z",
            "pretix_event_slug": "",
        },
    )
    assert activity_request.status_code == 200, activity_request.json
    activity_id2 = activity_request.json["id"]
    redirect_service.create_redirection(
        "/test2", f"http://svia.nl/activities/{activity_id2}"
    )

    assert (
        redirect_service.find_by_path("/test2").to
        == f"http://svia.nl/activities/{activity_id2}"
    )

    redirect.remove_expired_redirects()

    assert (
        redirect_service.find_by_path("/test2").to
        == f"http://svia.nl/activities/{activity_id2}"
    )


def test_redirect_removal_current(db, admin_client, admin_user):
    # Redirect started in the past (year 2000) and ends in the future (year 9999)
    activity_request = admin_client.post(
        "/api/activities/",
        json={
            "name": {"en": "asdfg", "nl": "asdfg"},
            "description": {"en": "adsfg", "nl": "asdfg"},
            "price": "2",
            "location": "Studievereniging VIA, Science Park 904, Amsterdam",
            "start_time": "2000-01-01T13:35:51.436Z",
            "end_time": "9999-02-02T13:35:51.436Z",
            "pretix_event_slug": "",
        },
    )

    assert activity_request.status_code == 200, activity_request.json

    activity_id3 = activity_request.json["id"]
    redirect_service.create_redirection(
        "/test3", f"http://svia.nl/activities/{activity_id3}"
    )

    assert (
        redirect_service.find_by_path("/test3").to
        == f"http://svia.nl/activities/{activity_id3}"
    )

    redirect.remove_expired_redirects()

    assert (
        redirect_service.find_by_path("/test3").to
        == f"http://svia.nl/activities/{activity_id3}"
    )


def test_redirect_removal_weird_urls(db, admin_client, admin_user):
    # Redirect started in the past (year 2000) and ends in the past (year 2000)
    activity_request = admin_client.post(
        "/api/activities/",
        json={
            "name": {"en": "asdfg", "nl": "asdfg"},
            "description": {"en": "adsfg", "nl": "asdfg"},
            "price": "2",
            "location": "Studievereniging VIA, Science Park 904, Amsterdam",
            "start_time": "2000-01-01T13:35:51.436Z",
            "end_time": "2000-02-02T13:35:51.436Z",
            "pretix_event_slug": "",
        },
    )

    assert activity_request.status_code == 200, activity_request.json

    activity_id = activity_request.json["id"]
    redirect_service.create_redirection(
        "/t1", f"http://svia.nl/activities/{activity_id}/"
    )
    redirect_service.create_redirection(
        "/t2", f"http://svia.nl/activities/{activity_id}"
    )
    redirect_service.create_redirection("/t3", f"svia.nl/activities/{activity_id}")
    redirect_service.create_redirection("/t4", f"/activities/{activity_id}")

    redirect.remove_expired_redirects()

    assert redirect_service.find_by_path("/t1") is None
    assert redirect_service.find_by_path("/t2") is None
    assert redirect_service.find_by_path("/t3") is None
    assert redirect_service.find_by_path("/t4") is None


def test_invalid_activities(db, admin_client, admin_user):
    redirect_service.create_redirection("/t1", "svia.nl/activities/999999999")
    redirect_service.create_redirection("/t2", "/activities/0")
    redirect_service.create_redirection("/t3", "/activities/")

    redirect.remove_expired_redirects()

    assert redirect_service.find_by_path("/t1") is None
    assert redirect_service.find_by_path("/t2") is None
    assert redirect_service.find_by_path("/t3").to == "/activities/"
