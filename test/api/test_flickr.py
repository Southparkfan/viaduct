import urllib.parse

import pytest
import requests_mock

from conftest import CustomClient


def match_flickr_method(method):
    def matcher(request):
        params = urllib.parse.parse_qs(request.text)
        if "method" not in params:
            return False
        if "format" not in params:
            return False
        if params["format"][0] != "json":
            return False
        return params["method"][0] == method

    return matcher


def test_list_albums(member_client, requests_mocker: requests_mock.Mocker):
    requests_mocker.post(
        "https://api.flickr.com/services/rest/",
        additional_matcher=match_flickr_method("flickr.photosets.getList"),
        # TODO Add actual photoset mock results, this is guestimation based on our code.
        json={
            "photosets": {
                "page": 1,
                "pages": 1,
                "perpage": 500,
                "total": 0,
                "photoset": [],
            },
            "stat": "ok",
        },
    )
    rv = member_client.get("/api/photos/")
    assert rv.status_code == 200


def test_find_album(member_client, requests_mocker: requests_mock.Mocker):
    album_id = "1"
    requests_mocker.post(
        "https://api.flickr.com/services/rest/",
        additional_matcher=match_flickr_method("flickr.photosets.getPhotos"),
        # TODO Add actual photo mock results, this is guestimation based on our code.
        json={
            "photoset": {
                "id": 1,
                "page": 1,
                "pages": 1,
                "perpage": 500,
                "primary": 0,
                "total": 0,
                "photo": [
                    {
                        "id": 1,
                        "farm": 1,
                        "secret": 123456,
                        "server": 1,
                        "title": "some photo",
                        "isprimary": 0,
                        "url_o": "https://example.com/url_o",
                    }
                ],
            },
            "stat": "ok",
        },
    )
    requests_mocker.post(
        "https://api.flickr.com/services/rest/",
        additional_matcher=match_flickr_method("flickr.photosets.getInfo"),
        # TODO Add actual photo mock results, this is guestimation based on our code.
        json={
            "photoset": {
                "date_create": 1,
                "title": {"_content": "title"},
                "description": {"_content": "description"},
                "farm": 1,
                "server": 1,
                "primary": 1,
                "secret": 1,
                "photos": 1,
            },
            "stat": "ok",
        },
    )
    rv = member_client.get(f"/api/photos/{album_id}")
    assert rv.status_code == 200


PHOTO_PATHS = ["/api/photos/", "/api/photos/random/", "/api/photos/123/"]


@pytest.mark.parametrize("path", PHOTO_PATHS)
def test_photo_module_permissions(unpaid_user_client: CustomClient, path) -> None:
    rv = unpaid_user_client.get(path)
    assert rv.status_code == 403


@pytest.mark.parametrize("path", PHOTO_PATHS)
def test_photo_module_permissions_anonymous(
    anonymous_client: CustomClient, path
) -> None:
    rv = anonymous_client.get(path)
    assert rv.status_code == 401
