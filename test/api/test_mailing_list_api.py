import pytest
from sqlalchemy.orm import Session

from app.models.mailinglist_model import MailingList
from app.models.user import User
from conftest import CustomClient


@pytest.fixture
def mailinglists(db_session):
    mailinglist1 = MailingList(
        nl_name="Nieuwsbrief",
        en_name="Newsletter",
        copernica_column_name="Ingeschreven",
        members_only=True,
    )

    mailinglist2 = MailingList(
        nl_name="Bedrijfsinformatie",
        en_name="Company information",
        copernica_column_name="Bedrijfsinformatie",
        members_only=True,
    )

    db_session.add(mailinglist1)
    db_session.add(mailinglist2)
    db_session.commit()

    return [mailinglist1, mailinglist2]


@pytest.fixture
def subscribed_user(db_session, mailinglists, user_factory):
    user: User = user_factory()
    user.mailinglists = mailinglists

    db_session.commit()

    return user


def test_list_all(admin_client: CustomClient, mailinglists: list[MailingList]):
    rv = admin_client.get("/api/mailinglists/")
    assert rv.status_code == 200

    content = rv.json
    assert len(content) == len(mailinglists)

    for i, ml in enumerate(mailinglists):
        assert content[i]["nl_name"] == ml.nl_name
        assert content[i]["en_name"] == ml.en_name


def test_get_single(admin_client: CustomClient, mailinglists: list[MailingList]):
    ml = mailinglists[0]
    rv = admin_client.get(f"/api/mailinglists/{ml.id}/")
    assert rv.status_code == 200

    content = rv.json

    assert content["nl_name"] == ml.nl_name
    assert content["en_name"] == ml.en_name


def test_get_single_nonexistent(admin_client: CustomClient):
    rv = admin_client.get("/api/mailinglists/123/")
    assert rv.status_code == 404


def test_create_new(admin_client: CustomClient, db_session):
    data = {
        "nl_name": "new NL name",
        "en_name": "new EN name",
        "nl_description": "new NL description",
        "en_description": "new EN description",
        "copernica_column_name": "some_column_name",
        "members_only": True,
        "default": False,
    }
    rv = admin_client.post("/api/mailinglists/", json=data)
    assert rv.status_code == 201

    ml = db_session.query(MailingList).filter_by(nl_name=data["nl_name"]).one_or_none()

    assert ml is not None
    assert ml.en_name == data["en_name"]
    assert ml.copernica_column_name == data["copernica_column_name"]
    assert ml.members_only == data["members_only"]
    assert ml.default == data["default"]


def test_create_new_invalid_copernica_column_format(admin_client: CustomClient):
    data = {
        "nl_name": "new NL name",
        "en_name": "new EN name",
        "nl_description": "new NL description",
        "en_description": "new EN description",
        "copernica_column_name": "some value with spaces",
        "members_only": True,
        "default": False,
    }

    rv = admin_client.post("/api/mailinglists/", json=data)
    assert rv.status_code == 409


def test_update(
    admin_client: CustomClient, mailinglists: list[MailingList], db_session
):
    ml = mailinglists[0]

    data = {
        "nl_name": "new NL name",
        "en_name": "new EN name",
        "nl_description": "new NL description",
        "en_description": "new EN description",
        "copernica_column_name": ml.copernica_column_name,
        "members_only": ml.members_only,
        "default": ml.default,
    }

    rv = admin_client.put(f"/api/mailinglists/{ml.id}/", json=data)
    assert rv.status_code == 200

    db_session.refresh(ml)
    assert ml.nl_name == data["nl_name"]
    assert ml.en_name == data["en_name"]


def test_delete(
    admin_client: CustomClient, mailinglists: list[MailingList], db_session
):
    ml = mailinglists[0]

    rv = admin_client.delete(f"/api/mailinglists/{ml.id}/")
    assert rv.status_code == 204

    ml = db_session.query(MailingList).filter_by(id=ml.id).one_or_none()
    assert ml is None


def test_delete_nonexistent(admin_client: CustomClient):
    rv = admin_client.delete("/api/mailinglists/123456/")
    assert rv.status_code == 404


def test_delete_subscribed_users(
    admin_client: CustomClient,
    subscribed_user: User,
    mailinglists: list[MailingList],
    db_session,
):
    ml = mailinglists[0]

    rv = admin_client.delete(f"/api/mailinglists/{ml.id}/")
    assert rv.status_code == 409

    ml = db_session.query(MailingList).filter_by(id=ml.id).one_or_none()
    assert ml is not None


def test_list_user_mailinglists(
    admin_client: CustomClient,
    subscribed_user: User,
    mailinglists: list[MailingList],
):
    rv = admin_client.get(f"/api/users/{subscribed_user.id}/mailinglist-subscriptions/")
    assert rv.status_code == 200, rv.json
    assert {ml["id"] for ml in rv.json} == {ml.id for ml in mailinglists}


def test_user_subscribe_mailinglist(
    member_client: CustomClient,
    member_user: User,
    mailinglists: list[MailingList],
    db_session: Session,
) -> None:
    assert not member_user.mailinglists.count()
    rv = member_client.post(
        "/api/users/self/mailinglist-subscriptions/",
        json={"id": mailinglists[0].id, "subscribed": True},
    )
    assert rv.status_code == 200, rv.json

    db_session.refresh(member_user)
    assert member_user.mailinglists.count()


def test_non_member_subscribe_member_mailinglist(
    unpaid_user_client: CustomClient,
    unpaid_user: User,
    mailinglists: list[MailingList],
    db_session: Session,
) -> None:
    assert not unpaid_user.mailinglists.count()
    assert mailinglists[0].members_only
    rv = unpaid_user_client.post(
        "/api/users/self/mailinglist-subscriptions/",
        json={"id": mailinglists[0].id, "subscribed": True},
    )

    assert rv.status_code == 409, rv.json
    assert rv.json["data"]["details"] == "Membership required"

    db_session.refresh(unpaid_user)
    assert not unpaid_user.mailinglists.count()


def test_non_member_unsubscribe_member_mailinglist(
    unpaid_user_client: CustomClient,
    unpaid_user: User,
    mailinglists: list[MailingList],
    db_session: Session,
) -> None:
    assert mailinglists[0].members_only
    unpaid_user.mailinglists.append(mailinglists[0])
    db_session.commit()

    rv = unpaid_user_client.post(
        "/api/users/self/mailinglist-subscriptions/",
        json={"id": mailinglists[0].id, "subscribed": False},
    )
    assert rv.status_code == 200, rv.json
    db_session.refresh(unpaid_user)

    assert not unpaid_user.mailinglists.count()
