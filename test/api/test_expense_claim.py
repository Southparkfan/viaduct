from unittest import mock

from conftest import CustomClient


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_expense_claim(mock, member_client: CustomClient):
    rv = member_client.post(
        "/api/declaration/send/",
        json={
            "file_data": [],
            "reason": "reason",
            "committee": "Commissie",
            "amount": "10.50",
            "iban": "GB33BUKB20201555555555",
        },
    )
    assert rv.status_code == 204, rv.json
    assert mock.called_once

    rv = member_client.get("/api/users/self/declarations/")
    assert rv.status_code == 200, rv.json
