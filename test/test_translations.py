from babel.messages.pofile import read_po


def test_translations():
    filenames = ["en/LC_MESSAGES/messages.po", "nl/LC_MESSAGES/messages.po"]
    for filename in filenames:
        with open(f"app/translations/{filename}", "rb") as f:
            catalog = read_po(f, domain="messages")
            assert not catalog.fuzzy, f"{filename} is fuzzy"
            for key, message in catalog._messages.items():
                assert not message.fuzzy, f"{key} is in {filename} is fuzzy"
