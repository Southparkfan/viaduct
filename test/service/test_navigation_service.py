import unittest

from app.service import navigation_service


class TestNavigationService(unittest.TestCase):
    def test_get_stripped_path(self):
        stripped_path1 = navigation_service.get_stripped_path("/bestuur12/", 0)
        self.assertEqual(stripped_path1, "bestuur12")

        stripped_path2 = navigation_service.get_stripped_path("/bestuur12", 0)
        self.assertEqual(stripped_path2, "bestuur")

        stripped_path3 = navigation_service.get_stripped_path("/activities/1337/", 0)
        self.assertEqual(stripped_path3, "activities/1337")

        stripped_path4 = navigation_service.get_stripped_path("/activities/1337", 0)
        self.assertEqual(stripped_path4, "activities")

        stripped_path5 = navigation_service.get_stripped_path("/user/avatar/1337/", 1)
        self.assertEqual(stripped_path5, "user/avatar")

        stripped_path6 = navigation_service.get_stripped_path("/user/avatar/1337", 1)
        self.assertEqual(stripped_path6, "user")
