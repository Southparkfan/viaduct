from unittest import TestCase, mock

import requests

from app.dev.requests import mocked_requests_get_json

JSON = {"test": "test"}


class TestMockedRequestsGetJson(TestCase):
    @mock.patch("requests.get", side_effect=mocked_requests_get_json(JSON, 200))
    def test_fetch_200(self, mock_get):
        mock_response = requests.get(1337)
        self.assertEqual(mock_response.json(), JSON)
        self.assertEqual(mock_response.status_code, 200)

    @mock.patch("requests.get", side_effect=mocked_requests_get_json(JSON, 404))
    def test_fetch_404(self, mock_get):
        mock_response = requests.get(1337)
        self.assertEqual(mock_response.json(), JSON)
        self.assertEqual(mock_response.status_code, 404)
