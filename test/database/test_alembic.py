from alembic.script import ScriptDirectory

from app.extensions import migrate


def test_single_head():
    script = ScriptDirectory.from_config(migrate.get_config())
    # This will raise if there are multiple heads
    script.get_current_head()
