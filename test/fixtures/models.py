import factory
import factory.fuzzy
import pytest
from sqlalchemy.orm import Session

from app.models.activity import Activity
from app.models.alv_model import Alv
from app.models.company import Company
from app.models.course import Course
from app.models.education import Education
from app.models.examination import Examination
from app.models.news import News
from app.models.pimpy import Minute
from app.models.redirect import Redirect
from app.models.user import User


@pytest.fixture
def education_factory(db_session):
    class EducationFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Education
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        nl_name = factory.Faker("name")
        en_name = factory.Faker("name")

        programme_type = factory.fuzzy.FuzzyChoice(
            ["BACHELOR", "MASTER", "MINOR", "OTHER"]
        )

        is_via_programme = factory.Faker("pybool")
        datanose_code = factory.Faker("pystr")

    return EducationFactory


@pytest.fixture
def user_factory(db_session, education_factory):
    class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = User
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        email = factory.Faker("email")
        password = factory.Faker("password")
        first_name = factory.Faker("first_name")
        last_name = factory.Faker("last_name")
        student_id = factory.Faker("pystr")
        student_id_confirmed = True
        educations = factory.RelatedFactoryList(education_factory, size=2)
        birth_date = factory.Faker("date", pattern="%Y-%m-%d")
        address = "Some street"
        zip = "1234FU"
        city = "Some city"
        country = "Nederland"
        phone_nr = ""
        alumnus = False
        favourer = False
        member_of_merit_date = None
        locale = "en"
        copernica_id = 1

    return UserFactory


@pytest.fixture(params=["active", "inactive"])
def company_factory(db_session, request):
    class CompanyFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Company
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        name = factory.Faker("company")

        # TODO REMOVE DESCRIPTION
        description = factory.Faker("text", max_nb_chars=200)
        logo_file_id = None
        website = factory.Faker("url")

        # Human readable string used in URL.
        slug = factory.Faker("slug")

        # Hard overwrite for the contract period.
        contract_start_date = factory.Faker("date_this_century", before_today=True)
        contract_end_date = factory.Faker(
            "date_this_century",
            before_today=request.param == "inactive",
            after_today=request.param == "active",
        )

    return CompanyFactory


@pytest.fixture
def redirect_factory(db_session):
    class RedirectFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Redirect
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        fro = factory.Faker("pystr", max_chars=200)
        to = factory.Faker("pystr", max_chars=200)

    return RedirectFactory


@pytest.fixture()
def activity_factory(db_session, admin_user):
    class ActivityFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Activity
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        nl_name = factory.Faker("text", max_nb_chars=40)
        en_name = factory.Faker("text", max_nb_chars=40)
        nl_description = factory.Faker("text", max_nb_chars=2000)
        en_description = factory.Faker("text", max_nb_chars=2000)

        start_time = factory.Faker("date_this_century", before_today=True)
        end_time = factory.Faker("date_this_century", after_today=True)

        location = factory.Faker("address")
        price = factory.Faker("pystr", max_chars=14)

    return ActivityFactory


@pytest.fixture
def news_factory(db_session, admin_user):
    class NewsFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = News
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        nl_title = factory.Faker("text", max_nb_chars=200)
        en_title = factory.Faker("text", max_nb_chars=200)
        nl_content = factory.Faker("text", max_nb_chars=2000)
        en_content = factory.Faker("text", max_nb_chars=2000)

        user_id = admin_user.id
        needs_paid = False
        publish_date = factory.Faker("date", pattern="%Y-%m-%d")

    return NewsFactory


@pytest.fixture
def minute_factory(db_session, admin_group):
    class MinuteFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Minute
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        content = factory.Faker("text", max_nb_chars=2000)
        group_id = admin_group.id
        minute_date = factory.Faker("date", pattern="%Y-%m-%d")

    return MinuteFactory


@pytest.fixture
def course_factory(db_session):
    class CourseFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Course
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        name = factory.Faker("first_name")
        datanose_code = "5384ADNE6Y"  # Advanced Networking

    return CourseFactory


@pytest.fixture
def examination_factory(db_session, course_factory):
    class ExaminationFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Examination
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        date = factory.Faker("date", pattern="%Y-%m-%d")
        comment = ""
        course = factory.RelatedFactory(course_factory)
        test_type = "Mid-term"

    return ExaminationFactory


@pytest.fixture
def alv_factory(db_session: Session, admin_user: User, activity_factory):
    class AlvFactory(factory.alchemy.SQLAlchemyModelFactory):
        class Meta:
            model = Alv
            sqlalchemy_session = db_session  # the SQLAlchemy session object
            sqlalchemy_session_persistence = "commit"

        nl_name = factory.Faker("name")
        en_name = factory.Faker("name")
        date = factory.Faker("date", pattern="%Y-%m-%d")

        activity = factory.RelatedFactory(activity_factory)

        chairman_user_id = admin_user.id
        secretary_user_id = admin_user.id

        minutes_file_id = None

    return AlvFactory
