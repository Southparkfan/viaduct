#!/bin/bash

set -euo pipefail

echo "Loading extensions"

HOST_ARGUMENT=()
if [[ -v "POSTGRES_HOST" ]]; then
  HOST_ARGUMENT+=("--host" "$POSTGRES_HOST")
fi

if [[ -v "POSTGRES_PASSWORD" ]]; then
  export PGPASSWORD="$POSTGRES_PASSWORD"
fi

psql -v ON_ERROR_STOP=1 "${HOST_ARGUMENT[@]}" --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE EXTENSION IF NOT EXISTS citext;
EOSQL
