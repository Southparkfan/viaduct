#!/bin/bash
# source: http://stackoverflow.com/questions/9023164/in-bash-how-can-i-run-multiple-infinitely-running-commands-and-cancel-them-all

npm run dev &
PIDS[0]=$!

if [ "$1" != "assets" ]; then
    flask run --host 0.0.0.0 &
    PIDS[1]=$!
fi

trap "kill -INT ${PIDS[*]}" SIGINT
trap "kill -TERM ${PIDS[*]}" SIGTERM
wait
