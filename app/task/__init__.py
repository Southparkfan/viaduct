from .user import update_most_historic_user_educations  # noqa
from .redirect import remove_expired_redirects  # noqa


import logging

# Log all registered period tasks to make sure they are imported.
_logger = logging.getLogger(__name__)
