import logging

from celery import states

from app import app, worker
from app.exceptions.base import (
    BusinessRuleException,
    DuplicateResourceException,
    ResourceNotFoundException,
    ValidationException,
)
from app.service import course_service, datanose_service, education_service

_logger = logging.getLogger(__name__)


@worker.task(bind=True)
def sync_datanose_courses(self) -> None:
    """Synchronizes all courses that are linked to a known Datanose education."""
    with app.app_context():
        year = datanose_service.get_current_academic_year()
        updated_ids = set()

        # Only update all Information Sciences related educations, or studies
        # from other educations that can be followed by an Information Sciences
        # education.
        educations = education_service.get_all_educations()
        educations_set = {e.datanose_code for e in educations}

        # Update all live courses from Datanose.
        for i, education in enumerate(educations):
            _logger.debug(
                "[%d/%d] Updating education %s's courses",
                i,
                len(educations) + 1,
                education.en_name,
            )
            self.update_state(
                state=states.STARTED, meta={"count": len(educations) + 1, "current": i}
            )
            courses = datanose_service._execute_get_request(
                f"/{year}/Faculty/FNWI/Programmes/{education.datanose_code}/Courses"
            )

            for course in courses:
                # Skip all courses without Datanose code.
                if not course["CatalogNumber"]:
                    continue

                # Parse the course into our data format.
                course = datanose_service._parse_course_programmes(course)
                c_programmes_set = {p["ProgrammeCode"] for p in course["Programmes"]}

                # Create course if a programme in the VIA database is associated.
                if educations_set.intersection(c_programmes_set):
                    try:
                        db_course = course_service.get_course(
                            datanose_code=course["CatalogNumber"]
                        )
                        course_service.update_education_course_links(
                            education, db_course, courses
                        )
                    except ResourceNotFoundException:
                        # Only add if the course is not yet in the database.
                        try:
                            db_course = course_service.add_course_using_datanose(
                                course["CatalogNumber"],
                                courses,
                            )
                        except (
                            DuplicateResourceException,
                            ValidationException,
                            BusinessRuleException,
                        ):
                            continue

                    # Add tuples for the main education and all bounded programmes.
                    updated_ids.add((education.id, db_course.id))

                    if course["Programmes"]:
                        for programme in course["Programmes"]:
                            try:
                                education = (
                                    education_service.get_education_by_datanose_code(
                                        programme["ProgrammeCode"]
                                    )
                                )
                                if education.is_via_programme:
                                    updated_ids.add((education.id, db_course.id))
                            except ResourceNotFoundException:
                                continue

        # Remove year and periods for discontinued courses.
        _logger.debug(
            "[%d/%d] Removing year and periods for discontinued courses",
            len(educations),
            len(educations) + 1,
        )
        self.update_state(
            state=states.STARTED,
            meta={"count": len(educations) + 1, "current": len(educations)},
        )
        education_ids = [e.id for e in educations]
        education_course_to_update = (
            course_service.get_education_course_by_exclusion_ids(
                list(updated_ids), education_ids
            )
        )

        for education_id, course_id in education_course_to_update:
            course_service.update_education_course_year_periods(
                education_id, course_id, None, None
            )
