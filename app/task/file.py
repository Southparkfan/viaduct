import logging
import os
import re

from flask import url_for
from markdown.inlinepatterns import IMAGE_LINK_RE, LINK_RE
from werkzeug.datastructures import FileStorage

from app import app, db, worker
from app.enums import FileCategory
from app.models.file import File
from app.service import file_service, page_service

_logger = logging.getLogger(__name__)
_re_link = re.compile(r"^(.*?)%s(.*)$" % LINK_RE, re.DOTALL | re.UNICODE)
_re_img = re.compile(r"^(.*?)%s(.*)$" % IMAGE_LINK_RE, re.DOTALL | re.UNICODE)


def replace_links_with_file(
    content: str | None, hashfs_file: File, query: str
) -> str | None:
    if content is None:
        return None

    new_lines = []

    current_url = url_for("static", filename=query)
    new_url = url_for("file.content", file=hashfs_file, file_hash=hashfs_file.hash)
    for line in content.splitlines():
        if current_url in line:
            new_lines.append(line.replace(current_url, new_url))
        else:
            new_lines.append(line)
    return "\n".join(new_lines)


@worker.task
def move_file_into_hash_fs(filename: str, environ):
    # We create a test_request_context instead of application context so
    # that we can use url_for in the replacement process.
    with app.test_request_context(**environ):
        path = f"./app/static/{filename}"
        if not os.path.exists(path):
            _logger.error("Cannot find file: %s", path)
            return

        # Add a lock so large page revisions do not have read before write.
        with app.redis.lock("move_file_into_hash_fs"):
            pages = page_service.find_latest_revisions_containing(filename)

            if not pages:
                # Do not create new hashfs if no revision are found using it.
                # Delete the old file.
                _logger.warning("Normally, delete file: %s", path)
                # os.remove(path)
                return

            with open(path, "rb") as f:
                file_storage = FileStorage(f, filename)

                hashfs_file = file_service.add_file(FileCategory.UPLOADS, file_storage)

            for page in pages:
                page.nl_content = replace_links_with_file(
                    page.nl_content, hashfs_file, filename
                )

                page.en_content = replace_links_with_file(
                    page.en_content, hashfs_file, filename
                )

                db.session.add(page)
                db.session.commit()

            # Delete the old file.
            _logger.warning("Normally, delete file: %s", path)
            # os.remove(path)
