import logging

from googleapiclient.errors import HttpError

from app import worker
from app.utils.google import build_groups_service

_logger = logging.getLogger(__name__)


@worker.task
def add_email_to_google_group(email: str, group_key: str) -> None:
    service = build_groups_service()
    if service is None:
        return
    try:
        _logger.info("Adding '%s' to google group '%s'", email, group_key)
        service.members().insert(
            groupKey=group_key, body={"email": email, "role": "MEMBER"}
        ).execute()
    except HttpError as e:
        if e.resp.status == 404:
            # Only handle user not found, raise on group not found.
            if (
                e.error_details
                and e.error_details[0].get("reason") == "notFound"
                and email in e.error_details[0].get("message")
            ):
                # TODO Notify user his e-mail was not configured in Google Drive
                _logger.warning("Email '%s' is not a known Google-account.", email)
                return
        elif (
            e.resp.status == 409
            and e.error_details
            and e.error_details[0].get("reason") == "duplicate"
        ):
            # The user is already in this group.
            _logger.warning(
                "Email '%s' already in google group '%s'.", email, group_key
            )
            return
        raise e


@worker.task
def remove_email_from_google_group(email: str, group_key: str) -> None:
    service = build_groups_service()
    if service is None:
        return
    try:
        _logger.info("Removing '%s' from google group '%s'", email, group_key)
        service.members().delete(groupKey=group_key, memberKey=email).execute()
    except HttpError as e:
        if e.resp.status == 404:
            if (
                e.error_details
                and e.error_details[0].get("reason") == "notFound"
                and "memberKey" in e.error_details[0].get("message")
            ):
                _logger.warning("'%s' is not a member of '%s' (404)", email, group_key)
                return
        raise e
