from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from flask_babel import _
from flask_login import current_user, login_required
from flask_wtf import FlaskForm

from app import get_locale
from app.decorators import require_membership
from app.roles import Roles
from app.service import membership_service, page_service, role_service
from app.service.education_service import get_education_name
from app.service.page_service import get_page_text_for_path
from app.views.viewmodel.membership import (
    disable_membership_confirmation_pending_banner,
)

blueprint = Blueprint("membership", __name__, url_prefix="/membership")


@blueprint.route("/payment/")
@disable_membership_confirmation_pending_banner
@login_required
def payment():
    if current_user.has_paid:
        return redirect(url_for("membership.complete"))

    page_text = get_page_text_for_path("/membership/payment/")
    page_text_fail = get_page_text_for_path("/membership/payment/cannot/")

    user = current_user

    params = {
        "can_pay": False,
        "page_text": page_text,
        "page_text_fail": page_text_fail,
        "fee": -1,
        "is_already_paid": False,
        "is_alumnus": False,
        "is_student_id_not_confirmed": False,
        "educations": [],
    }

    if not membership_service.can_pay_membership(user):
        params["is_already_paid"] = user.has_paid
        params["is_alumnus"] = user.alumnus
        params["is_student_id_not_confirmed"] = not user.student_id_confirmed
        params["educations"] = [
            get_education_name(edu, get_locale()) for edu in user.educations
        ]
    else:
        fee = membership_service.get_membership_price_from_via_studies(user.educations)
        if fee == 0:
            membership_service.confirm_free_membership(user)
            return redirect(url_for("membership.complete"))

        params["can_pay"] = True
        params["fee"] = fee
        params["pay_form"] = FlaskForm()
        params["pay_form_submit_text"] = _("Pay membership fee (€ {:,.2f})").format(
            fee / 100
        )

    return render_template("membership/payment.htm", **params)


@blueprint.route("/pay/", methods=["POST"])
@disable_membership_confirmation_pending_banner
def pay():
    if current_user.has_paid:
        return redirect(url_for("membership.complete"))

    # We only use it for csrf token checking.
    if not FlaskForm().validate_on_submit():
        abort(400)

    transaction, checkout_url = membership_service.create_membership_transaction(
        current_user
    )

    return redirect(checkout_url)


@blueprint.route("/complete/")
@require_membership
def complete():
    if not membership_service.is_any_study_from_via(current_user.educations):
        fee = None
    else:
        fee = membership_service.get_membership_price_from_via_studies(
            current_user.educations
        )

    page_text = (
        get_page_text_for_path("/membership/complete/normal")
        if fee != 0
        else get_page_text_for_path("/membership/complete/free")
    )

    return render_template("membership/paid.htm", page_text=page_text, fee=fee)


@blueprint.route("/complete/normal/")
@blueprint.route("/complete/free/")
@blueprint.route("/payment/cannot/")
def reserved_page():
    page = page_service.get_page_by_path(request.path)
    if not role_service.user_has_role(current_user, Roles.PAGE_WRITE):
        abort(403)

    flash(_("This page is not publicly available, but used in sign-up."))
    revision = page_service.get_latest_revision(page)
    return render_template(
        "page/view_single.htm",
        page=page,
        revision=revision,
        title=_("Membership payment"),
        can_write=True,
    )
