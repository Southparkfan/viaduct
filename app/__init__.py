import json
import logging.config
import mimetypes
import os
from typing import Any

import celery.signals
import pydantic
import sentry_sdk
from celery import Celery
from flask import Flask, has_request_context, jsonify, request, session
from flask_login import current_user
from hashfs import HashFS
from pydantic import SecretStr
from redis import Redis
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.redis import RedisIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
from sqlalchemy import event, exc

from app import constants
from app.extensions import babel, db
from app.utils.flask_init import FlaskInitializer
from app.utils.git_version import GitVersion

# Load the git hash from the .git folder. This works, since the docker
# image build in gitlab also contain the .git folder.
from config import SqlalchemyPostgresDsn

version: GitVersion = GitVersion()


class ViaFlask(Flask):
    redis: Redis

    def make_response(self, rv: Any):
        if isinstance(rv, list):
            rv = jsonify(rv)
        return super().make_response(rv)


app = ViaFlask(__name__)

# Authlib: Also generate refresh tokens in /oauth/token/
app.config["OAUTH2_REFRESH_TOKEN_GENERATOR"] = True

# Flask: do not sort json keys.
app.config["JSON_SORT_KEYS"] = False

# Flask-Sqlalchemy: Disable tracking modification overhead warning.
#  It is used for custom Flask-Sqlalchemy signals, we do not use
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Flask-Login: This config is used to set the login to expire on inactivity.
app.config["REMEMBER_COOKIE_REFRESH_EACH_REQUEST"] = True

# Start using an Sqlalchemy 2.0 compatible session.
app.config["SQLALCHEMY_ENGINE_OPTIONS"] = {
    "future": True,
}

app_init = FlaskInitializer(app)

# Locate config file relative to this file, instead of based on working directory.
logging_conf_file = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), "logging.conf"
)
logging.config.fileConfig(logging_conf_file, disable_existing_loggers=False)

_logger = logging.getLogger(__name__)
_logger.info("Git hash: %s", version.hash)
_logger.info("Git date: %s", version.timestamp.isoformat())

hashfs = HashFS("app/uploads/")
mimetypes.init()

worker: Celery = Celery("viaduct")


def static_url(url):
    return url + "?v=" + version.hash


@babel.localeselector
def get_locale():
    languages = list(constants.LANGUAGES.keys())

    if not has_request_context():
        return languages[0]
    # Try to look-up an session set for language
    lang = session.get("lang")
    if lang and lang in languages:
        return lang

    # if a user is logged in, use the locale from the user settings
    if current_user.is_authenticated and current_user.locale is not None:
        return current_user.locale

    return request.accept_languages.best_match(languages, default="nl")


class ApplicationConnectionUrl(pydantic.BaseSettings):
    sqlalchemy_database_uri: SqlalchemyPostgresDsn
    broker_url: pydantic.RedisDsn


connection_urls = ApplicationConnectionUrl()
app.config["SQLALCHEMY_DATABASE_URI"] = connection_urls.sqlalchemy_database_uri
db.init_app(app)


@celery.signals.worker_process_init.connect
def reload_connection_pools(*args, **kwargs):
    """
    Delete all the connections in the connection pool after forking.

    During application initialization some database queries are executed. This hook
    will make sure that the pool is emptied in all the uwsgi worker processes, to
    prevent a single connection (file descriptor) being used by concurrent requests.
    """
    with app.app_context():
        _logger.info("Reloading connection pools [pid=%d]", os.getpid())
        db.engine.dispose()


try:
    import uwsgidecorators

    uwsgidecorators.postfork(reload_connection_pools)
except ImportError:
    # Whenever this is imported in development or in the entrypoint, the `uwsgi`
    # module is not available.
    pass


with app.app_context():

    @event.listens_for(db.engine, "connect")
    def connect(dbapi_connection, connection_record):
        """Save the current process id."""
        connection_record.info["pid"] = os.getpid()

    @event.listens_for(db.engine, "checkout")
    def checkout(dbapi_connection, connection_record, connection_proxy):
        """
        Assert reused db connections from the pool are from same process.

        This should not trigger, since we clear the connection pool after uwsgi prefork.
        However, we leave this hook to ensure we do not run into it in the future.
        """
        pid = os.getpid()
        if connection_record.info["pid"] != pid:
            connection_record.connection = connection_proxy.connection = None
            error = (
                "Connection record belongs to pid %s, attempting to check out "
                "in pid %s"
            )
            _logger.error(error, connection_record.info["pid"], pid)
            # Causes the connection to be removed from the pool.
            raise exc.DisconnectionError(error % (connection_record.info["pid"], pid))


app_init.init_converters()
app_init.init_views()
app_init.init_migrate(db)
from app import cli  # noqa

from app.service.setting_service import DatabaseSettingsMixin  # noqa


class FlaskConfigSettings(DatabaseSettingsMixin):
    SECRET_KEY = SecretStr("defaultsecretkeythatshouldbeoverwritten")
    RECAPTCHA_PUBLIC_KEY = "6LeQohoTAAAAANLwESlrlL733JlK300F2unWHhZk"
    RECAPTCHA_PRIVATE_KEY = SecretStr("")

    class Config:
        # Allow json encoding of this struct, to be able to load it directly in
        # the app.config.
        json_encoders = {
            SecretStr: lambda v: v.get_secret_value() if v else None,
        }


class SentrySettings(DatabaseSettingsMixin):
    sentry_dsn = SecretStr("")
    environment: str = "Development"


def init_app() -> Flask:
    # Workarounds for template reloading
    app.jinja_env.auto_reload = app.debug
    app.config["TEMPLATES_AUTO_RELOAD"] = app.debug

    # Disable cookies on http (and thereby prevent hijack the user session)
    app.config["SESSION_COOKIE_SECURE"] = not app.debug

    _logger.info("Loading additional database config")
    # TODO Improve accessing SQLAlchemy engine/session without flask app context.
    with app.app_context():
        flask_config = FlaskConfigSettings(db_session=db.session)
        app.config.from_mapping(json.loads(flask_config.json()))

    # Has to be imported *after* app is created and Babel is initialised
    from app import jinja_env  # noqa

    app_init.init_worker(connection_urls.broker_url)
    app_init.init_babel()
    app_init.init_cors()
    app_init.init_login()
    app_init.init_oauth()

    # TODO Improve accessing SQLAlchemy engine/session without flask app context.
    with app.app_context():
        sentry_settings = SentrySettings(db_session=db.session)

    if not app.debug and sentry_settings.sentry_dsn:
        sentry_sdk.init(
            dsn=sentry_settings.sentry_dsn.get_secret_value(),
            integrations=[
                FlaskIntegration(),
                RedisIntegration(),
                SqlalchemyIntegration(),
                CeleryIntegration(),
            ],
            release=version.hash,
            environment=sentry_settings.environment,
            send_default_pii=True,
        )

    from app.utils import context_filters  # noqa
    from app.utils.json import JSONEncoder

    app.json_encoder = JSONEncoder

    return app
