import enum
import re

from flask_babel import lazy_gettext as _
from sqlalchemy import Column, Date, Enum, Integer, String
from sqlalchemy.orm import backref, relationship
from sqlalchemy.schema import ForeignKey

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.course import Course
from app.models.file import File


class ExamFileType(enum.Enum):
    EXAMINATION = 1
    ANSWERS = 2
    SUMMARY = 3


test_types = {
    "Mid-term": _("Mid-Term"),
    "End-term": _("End-Term"),
    "Retake": _("Retake"),
    "Unknown": _("Unknown"),
}

test_type_default = "Unknown"


@mapper_registry.mapped
class Examination(BaseEntity):
    __tablename__ = "examination"

    comment = Column(String(128), nullable=False, default="")
    date = Column(Date)

    examination_file_id = Column(Integer, ForeignKey("file.id"))
    answers_file_id = Column(Integer, ForeignKey("file.id"))
    summary_file_id = Column(Integer, ForeignKey("file.id"))

    course_id = Column(Integer, ForeignKey("course.id"))
    test_type = Column(
        Enum(*list(test_types.keys()), name="examination_test_type"),
        nullable=False,
        server_default="Unknown",
    )
    course: Course = relationship(
        Course, backref=backref("examinations", lazy="dynamic")
    )

    examination_file: File = relationship(
        File, foreign_keys=[examination_file_id], lazy="joined"
    )
    answers_file: File = relationship(
        File, foreign_keys=[answers_file_id], lazy="joined"
    )
    summary_file: File = relationship(
        File, foreign_keys=[summary_file_id], lazy="joined"
    )

    def _get_filename(self, file_type):
        fn = ""

        for word in re.split(r"\s+", self.course.name):
            fn += word.capitalize()

        if self.test_type == "Mid-term":
            fn += "_Midterm"
        elif self.test_type == "End-term":
            fn += "_Final"
        elif self.test_type == "Retake":
            fn += "_Retake"

        if self.date is not None:
            fn += self.date.strftime("_%d_%m_%Y")

        if file_type is ExamFileType.EXAMINATION:
            fn += f".{self.examination_file.extension}"
        elif file_type is ExamFileType.ANSWERS:
            fn += f"_answers.{self.answers_file.extension}"
        elif file_type is ExamFileType.SUMMARY:
            fn += f"_summary.{self.summary_file.extension}"

        return fn

    @property
    def examination_filename(self):
        """
        Filename for the examination file (without extension).

        Create a filename for the examination file based on the exam's
        information.
        """
        return self._get_filename(ExamFileType.EXAMINATION)

    @property
    def answers_filename(self):
        """
        Filename for the answers file (without extension).

        Create a filename for the answers file based on the exam's information.
        """

        if self.answers_file is None:
            return None
        return self._get_filename(ExamFileType.ANSWERS)

    @property
    def summary_filename(self):
        """
        Filename for the summary file (without extension).

        Create a filename for the summary file based on the exam's information.
        """

        if self.summary_file is None:
            return None
        return self._get_filename(ExamFileType.SUMMARY)
