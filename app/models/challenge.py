import datetime

import pytz
from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    Enum,
    ForeignKey,
    Integer,
    String,
    Text,
)
from sqlalchemy.orm import backref, relationship

from app import constants
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.user import User


def dump_datetime(value):
    """Deserialize datetime object into string form for JSON processing."""
    if value is None:
        return None
    return [
        value.strftime(constants.DATE_FORMAT),
        value.strftime(constants.TIME_FORMAT),
    ]


@mapper_registry.mapped
class Challenge(BaseEntity):
    __tablename__ = "challenge"

    name = Column(String(200), unique=True)
    description = Column(Text())
    hint = Column(String(1024))
    start_date = Column(DateTime(timezone=True))
    end_date = Column(DateTime(timezone=True))
    parent_id = Column(Integer)
    weight = Column(Integer)
    answer = Column(Text())
    type = Column(Enum("Text", "Image", "Custom", name="challenge_type"))

    @property
    def open(self):
        tz = pytz.timezone("Europe/Amsterdam")
        now = datetime.datetime.now(tz=tz)
        return self.start_date <= now <= self.end_date


@mapper_registry.mapped
class Submission(BaseEntity):
    __tablename__ = "submission"

    challenge_id = Column(Integer, ForeignKey("challenge.id"))
    user_id = Column(Integer, ForeignKey("user.id"))
    user: User = relationship("User", backref=backref("submission", lazy="dynamic"))
    answer = Column(Text())
    image_path = Column(String(256))
    approved = Column(Boolean)


@mapper_registry.mapped
class Competitor(BaseEntity):
    __tablename__ = "competitor"

    user_id = Column(Integer, ForeignKey("user.id"))
    user: User = relationship("User")
    points = Column(Integer)

    @property
    def serialize(self):
        """Return object data in easily serializeable format."""
        return {
            "name": self.user.name,
            "points": self.points,
        }
