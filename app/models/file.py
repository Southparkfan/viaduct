from sqlalchemy import Column, Enum, String, UniqueConstraint
from sqlalchemy.orm import Mapped

from app.enums import FileCategory
from app.extensions import mapper_registry
from app.models.base_model import BaseEntity


@mapper_registry.mapped
class File(BaseEntity):
    """Contains the metadata of an uploaded file."""

    __tablename__ = "file"

    # Hash has a historical reason: we use(d) HashFS to store files
    # on the server directly.
    # When files are stored in S3, this field contains a UUID.
    hash: Mapped[str] = Column(String(200), nullable=False)
    extension: Mapped[str] = Column(String(20), nullable=False)

    category: Mapped[FileCategory] = Column(
        Enum(FileCategory, name="file_category"), nullable=False
    )
    display_name: Mapped[str] = Column(String(200))

    # TODO This should be nullable False once we have everything in S3.
    s3_bucket_id: Mapped[str] = Column(String(256), nullable=True)

    @property
    def full_display_name(self):
        if not self.display_name:
            return None

        name = self.display_name
        if self.extension:
            name += "." + self.extension

        return name

    @property
    def require_membership(self):
        return self.category in {
            FileCategory.UPLOADS_MEMBER,
            FileCategory.ALV_DOCUMENT,
            FileCategory.EXAMINATION,
        }

    # TODO Drop this constraint, it makes no sense!
    __table_args__ = (UniqueConstraint("display_name", "extension"),)
