"""
Extra functionality that is used by all models.

It extends db.Model with extra functions.
"""
from datetime import datetime, timezone
from typing import Any

from sqlalchemy import Column, DateTime, Integer
from sqlalchemy.orm import declarative_mixin


@declarative_mixin
class BaseEntity:
    __table_args__: Any = {"sqlite_autoincrement": True}

    # Columns (in order) to be printed when an instance of the object is
    # printed
    prints: tuple[str, ...] = ("id",)

    # Columns that every model needs
    id: int = Column(Integer, primary_key=True)
    created: datetime = Column(
        DateTime(timezone=True),
        default=lambda: datetime.now(timezone.utc),
        nullable=False,
    )
    modified: datetime = Column(
        DateTime(timezone=True),
        default=lambda: datetime.now(timezone.utc),
        onupdate=lambda: datetime.now(timezone.utc),
        nullable=False,
    )

    # Function used by print to print a model at server side.
    # It uses the prints attribute from the object to determine what values to
    # print. This attribute is the id of the object by default.
    def __repr__(self):
        first = True
        string = "<%s(" % type(self).__name__

        for attr in self.prints:
            if not first:
                string += ", "
            string += '"%s"' % (getattr(self, attr))
            first = False

        string += ")>"

        return string
