from authlib.integrations.sqla_oauth2 import OAuth2ClientMixin
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.user import User


@mapper_registry.mapped
class OAuthClient(OAuth2ClientMixin):
    __tablename__ = "oauth_client"

    # Overwrite the mixin client_id, since we want it to be the primary key.
    client_id = Column(String(48), primary_key=True)

    # Creator of the client
    user: User = relationship("User")
    user_id = Column(Integer, ForeignKey("user.id"))

    auto_approve = Column(Boolean(), default=False, nullable=False)

    @hybrid_property
    def scopes(self):
        if self.scope:
            return self.scope.splitlines()
        return []
