from typing import TYPE_CHECKING, Any

from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import backref, relationship
from sqlalchemy.schema import ForeignKey

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.education import Education  # noqa


@mapper_registry.mapped
class EducationCourse:
    __tablename__ = "education_course"
    education_id = Column(Integer, ForeignKey("education.id"), primary_key=True)
    course_id = Column(Integer, ForeignKey("course.id"), primary_key=True)
    year = Column(Integer, nullable=True)
    periods = Column(postgresql.ARRAY(Integer), nullable=True)


@mapper_registry.mapped
class Course(BaseEntity):
    __tablename__ = "course"
    name: str = Column(String(128), nullable=False)
    datanose_code: str = Column(String(256), nullable=False)

    educations: Any = relationship(
        "Education",
        secondary=EducationCourse.__table__,
        backref=backref("courses", lazy="dynamic"),
        lazy="dynamic",
    )
