from typing import TYPE_CHECKING, Any

from sqlalchemy import Boolean, Column, String, Text
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.user import UserMailinglist

if TYPE_CHECKING:
    from app.models.user import User  # noqa


@mapper_registry.mapped
class MailingList(BaseEntity):
    __tablename__ = "mailing_list"
    nl_name: str = Column(String(64), nullable=False, unique=True)
    en_name: str = Column(String(64), nullable=False, unique=True)
    nl_description = Column(Text(), nullable=False, server_default="")
    en_description = Column(Text(), nullable=False, server_default="")
    copernica_column_name: str = Column(String(64), nullable=False, unique=True)
    members_only = Column(Boolean(), nullable=False)

    default = Column(Boolean(), nullable=False, default=False)

    # TODO https://github.com/sqlalchemy/sqlalchemy/issues/6229
    users: Any = relationship(
        "User",
        uselist=True,
        secondary=UserMailinglist.__table__,
        back_populates="mailinglists",
        lazy="dynamic",
    )
