from sqlalchemy import Column, DateTime, ForeignKey, Integer, Text
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.course import Course
from app.models.user import User


@mapper_registry.mapped
class Tutoring(BaseEntity):
    __tablename__ = "tutoring"

    deleted = Column(DateTime(timezone=True), nullable=True)

    num_hours = Column(Integer(), nullable=False)
    notes = Column(Text, nullable=False, server_default="")
    course_id = Column(Integer, ForeignKey("course.id"), nullable=False)
    course: "Course" = relationship("Course")

    tutor_id = Column(Integer, ForeignKey("user.id"), nullable=True)
    tutor: User = relationship("User", foreign_keys=[tutor_id])
    tutee_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    tutee: User = relationship("User", foreign_keys=[tutee_id])


@mapper_registry.mapped
class Tutor(BaseEntity):
    __tablename__ = "tutor"

    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    user: User = relationship("User")

    course_id = Column(Integer, ForeignKey("course.id"), nullable=False)
    course: Course = relationship("Course")

    approved = Column(DateTime(timezone=True))
    grade = Column(Integer(), nullable=False)
