from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity

if TYPE_CHECKING:
    from app.models.file import File


@mapper_registry.mapped
class DOMjudgeContestSettings(BaseEntity):
    __tablename__ = "domjudge_contest_settings"

    # Contests are no foreign keys because they life at domjudge.svia.nl.
    contest_id: int = Column(Integer, nullable=False, unique=True)
    banner_file_id = Column(Integer, ForeignKey("file.id"), nullable=True)
    banner_url = Column(String(512), nullable=True)

    banner_file: "File" = relationship("File", foreign_keys=[banner_file_id])
