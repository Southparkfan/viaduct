import logging

from app import app, db
from app.service.setting_service import DatabaseSettingsMixin
from app.task.mail import MailCommand, send_mail_task

_logger = logging.getLogger(__name__)


class DeveloperMailSetting(DatabaseSettingsMixin):
    developer_mail: str = ""


def send_mail(command: MailCommand):
    settings = DeveloperMailSetting(db_session=db.session)
    if app.debug or settings.developer_mail:
        command.html = (
            f"<p>DEBUGGING: {command.to} was original address</p>" + command.html
        )
        if not settings.developer_mail:
            _logger.warning("Missing DEVELOPER_MAIL in settings.")
            for line in command.html.splitlines():
                _logger.debug(line)
            return
        else:
            command.to = settings.developer_mail

    command.validate()

    send_mail_task.delay(command)
