import logging
from typing import IO

import boto3
from pydantic import BaseSettings

from app.exceptions.base import ResourceNotFoundException, ServiceUnavailableException
from app.models.file import File

logger = logging.getLogger(__name__)


class S3Config(BaseSettings):
    s3_endpoint = "s3.eu-central-003.backblazeb2.com"
    s3_bucket = "viaduct-files"
    aws_access_key_id = "backblaze_keyID"
    aws_secret_access_key = "backblaze_applicationKey"


def get_public_url_for_file(key: str) -> str:
    config = S3Config()
    return f"https://{config.s3_bucket}.{config.s3_endpoint}/{key}"


def get_presigned_url_for_file(file_: File) -> str:
    config = S3Config()
    s3 = boto3.client("s3", endpoint_url=f"https://{config.s3_endpoint}")

    try:
        s3.head_object(Bucket=file_.s3_bucket_id, Key=file_.hash)
        logger.info(f'Found "{file_.hash}" in the bucket "{file_.s3_bucket_id}".')
        # TODO Generate presigned url.
        return ""
    except s3.exceptions.NoSuchKey:
        logger.info(f'Not found "{file_.hash}" in the bucket "{file_.s3_bucket_id}".')
        raise ResourceNotFoundException("file", file_.display_name)
    except Exception:
        logger.exception(f'Unable to find file "{file_.hash}"')
        raise ServiceUnavailableException("Generating file url failed", "s3")


def add_file_to_s3(key: str, blob: IO[bytes]) -> str:
    config = S3Config()
    s3 = boto3.client("s3", endpoint_url=f"https://{config.s3_endpoint}")

    try:
        s3.put_object(Body=blob, Bucket=config.s3_bucket, Key=key)
        logger.info(f"Uploaded in-memory file to S3: {config.s3_bucket}/{key}")
        return config.s3_bucket
    except s3.exceptions.S3UploadFailedError as e:
        logger.exception(f"Failed uploading file to S3: {config.s3_bucket}/{key}")
        raise ServiceUnavailableException("Failed to upload file to S3.", "s3") from e


def read_file_from_s3(key: str) -> IO[bytes]:
    config = S3Config()
    s3 = boto3.client("s3", endpoint_url=f"https://{config.s3_endpoint}")

    try:
        return s3.get_object(Bucket=config.s3_bucket, Key=key)["Body"]
    except s3.exceptions.NoSuchKey as e:
        logger.exception(f"Cannot find file in S3: {config.s3_bucket}/{key}")
        raise ResourceNotFoundException("s3", key) from e


def delete_file(key: str) -> None:
    config = S3Config()
    s3 = boto3.client("s3", endpoint_url=f"https://{config.s3_endpoint}")

    try:
        s3.delete_object(Bucket=config.s3_bucket, Key=key)
    except s3.exceptions.NoSuchKey:
        logger.warning(f"Cannot find+delete file in S3: {config.s3_bucket}/{key}")
