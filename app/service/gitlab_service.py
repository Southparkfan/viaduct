import enum
import logging
from typing import Literal
from unittest.mock import Mock

import requests
from flask import url_for
from pydantic import SecretStr

from app import db
from app.exceptions.base import ServiceUnavailableException
from app.models.user import User
from app.repository import gitlab_repository
from app.service.setting_service import DatabaseSettingsMixin

_logger = logging.getLogger(__name__)


class GitlabSettings(DatabaseSettingsMixin):
    gitlab_token = SecretStr("")


class GitlabProjectEnum(enum.IntEnum):
    viaduct = 4110282
    pos = 6644893
    pretix = 14120498


def create_gitlab_session(token: str) -> requests.Session:
    session = requests.Session()
    session.headers.update({"PRIVATE-TOKEN": token})
    return session


def create_gitlab_issue(
    project_name: Literal["viaduct", "pretix", "pos"],
    summary: str,
    user: User,
    description: str,
) -> str:
    project = GitlabProjectEnum[project_name]
    # With UserSelfConverter the flask.url_for(user=current_user) uses "self" in urls.
    # We use a mock user to ensure the identity check fails and the id is used instead.
    mock_user = Mock(spec_set=("id",))
    mock_user.id = user.id
    description = (
        f"Bug report by {user.first_name}, viaduct ID: [{user.id}]"
        f"({url_for('user.view_single_user', user=mock_user, _external=True)})"
        f"\n\n{description}"
    )
    data = {"title": summary, "description": description, "labels": "viaduct"}
    settings = GitlabSettings(db_session=db.session)
    with create_gitlab_session(settings.gitlab_token.get_secret_value()) as s:
        try:
            resp = gitlab_repository.create_gitlab_issue(s, project.value, data)
            resp.raise_for_status()
            return resp.json()
        except requests.RequestException as e:
            _logger.error(e, e.response.json() if e.response else "")
            raise ServiceUnavailableException("Gitlab unavailable") from e
