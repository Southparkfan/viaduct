from flask_sqlalchemy import Pagination

from app.api.schema import PageSearchParameters
from app.exceptions.base import (
    DuplicateResourceException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.redirect import Redirect
from app.repository import model_service, redirect_repository


def create_redirection(fro: str, to: str) -> type[Redirect]:
    if redirect_repository.redirect_exists(fro=fro):
        raise DuplicateResourceException("redirect", fro)

    r = Redirect(fro=fro, to=to)

    return model_service.save(r)


def get_all() -> list[Redirect]:
    return model_service.get_all(Redirect, Redirect.fro)


def paginated_search_all_redirects(pagination: PageSearchParameters) -> Pagination:
    return redirect_repository.paginated_search_all_groups(pagination)


def find_by_path(path: str) -> Redirect | None:
    return redirect_repository.find_by_path(path)


def get_by_path(path: str) -> Redirect:
    redirect = find_by_path(path)

    if redirect is None:
        raise ResourceNotFoundException("redirect", path)
    return redirect


def is_a_redirect_chain(redirects: list[Redirect], to: str) -> bool:
    for r in redirects:
        if r.fro.lower() == to.lower():
            return True

    return False


def upsert_redirection(fro: str, to: str) -> Redirect | type[Redirect]:
    if is_a_redirect_chain(get_all(), to):
        raise ValidationException("The destination URL is already a redirect")

    redirect = find_by_path(fro)

    if redirect:
        return redirect_repository.edit_redirection(redirect, fro, to)

    return create_redirection(fro, to)
