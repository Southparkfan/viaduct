import enum
import json
import logging
import os
from collections import defaultdict
from datetime import datetime, timedelta
from decimal import Decimal
from typing import Literal, NamedTuple, TypedDict
from urllib.parse import urljoin

import pydantic
import requests
from cryptography import fernet
from pydantic import SecretStr
from requests.structures import CaseInsensitiveDict

from app import app, db
from app.exceptions.base import ServiceUnavailableException
from app.models.user import User
from app.networking import DefaultResponseAdapter
from app.roles import Roles
from app.service import group_service, role_service, user_service
from app.service.setting_service import DatabaseSettingsMixin

_logger = logging.getLogger(__name__)

PRETIX_SESSION = requests.Session()


class PretixEventRequest(NamedTuple):
    name: dict[str, str]  # language -> event name
    date_from: datetime
    date_to: datetime
    quota_size: int
    group_id: int
    price: Decimal


class PretixSettings(DatabaseSettingsMixin):
    pretix_enabled: bool = False
    pretix_host: pydantic.HttpUrl = "https://pretix.svia.nl"  # type: ignore[assignment]
    pretix_token = SecretStr("")
    pretix_organizer: str = "via"


with app.app_context():
    settings = PretixSettings(db_session=db.session)

    PRETIX_SESSION.mount(settings.pretix_host, DefaultResponseAdapter("Pretix"))
    PRETIX_SESSION.headers = CaseInsensitiveDict(
        {
            "accept": "application/json",
            "content-Type": "application/json",
            "authorization": "Token " + settings.pretix_token.get_secret_value(),
        }
    )
    PRETIX_BASE_URL = urljoin(
        settings.pretix_host, "/api/v1/organizers/" + settings.pretix_organizer + "/"
    )


class PretixEventSettings(TypedDict, total=False):
    via_membership_required: bool
    via_favourer_allowed: bool


class PretixEventDetails(TypedDict):
    testmode: bool
    plugins: list[str]


class PretixEvent:
    def __init__(self, slug: str) -> None:
        self.slug = slug
        self.settings = get_event_settings(self.slug)
        self.details = get_event_details(self.slug)


def get_event(slug: str) -> PretixEvent:
    try:
        return PretixEvent(slug=slug)
    except requests.exceptions.HTTPError as e:
        _logger.warning("Pretix error %s", e)
        msg = e.response.reason
        try:
            msg = e.response.json().get("detail")
        except requests.exceptions.JSONDecodeError:
            pass
        raise ServiceUnavailableException(service="Pretix", details=msg) from e
    except requests.exceptions.ConnectionError as e:
        _logger.error("Pretix error %s", e)
        raise ServiceUnavailableException(
            service="Pretix", details="Unable to connect to Pretix"
        ) from e


def get_event_settings(slug: str) -> PretixEventSettings:
    request_path = urljoin(PRETIX_BASE_URL, f"events/{slug}/settings/")

    resp = PRETIX_SESSION.get(request_path)
    resp.raise_for_status()
    return json.loads(resp.content)


def get_event_details(slug: str) -> PretixEventDetails:
    request_path = urljoin(PRETIX_BASE_URL, f"events/{slug}/")
    resp = PRETIX_SESSION.get(request_path)
    resp.raise_for_status()
    return resp.json()


def get_events_for_user(user: User, args: dict) -> list:
    request_path = urljoin(PRETIX_BASE_URL, "events/")
    pretix_args = args.copy()

    if not role_service.user_has_role(user, Roles.FORM_ADMIN):
        groups = group_service.get_groups_for_user(user)
        group_ids = {str(group.id) for group in groups}

        pretix_args["group_id"] = ",".join(group_ids)

    resp = PRETIX_SESSION.get(request_path, params=pretix_args)
    if not resp.ok:
        return []
    return json.loads(resp.content)["results"]


class PretixOrder(NamedTuple):
    code: str
    secret: str


class PretixPaymentStatus(enum.IntFlag):
    # Official pretix stati
    paid = 1
    pending = 2
    canceled = 4
    expired = 8

    # Often requested together.
    active = paid | pending

    def strings(self) -> tuple[str, ...]:
        stati = {
            PretixPaymentStatus.paid: "p",
            PretixPaymentStatus.pending: "n",
            PretixPaymentStatus.canceled: "c",
            PretixPaymentStatus.expired: "e",
        }
        r = []
        for status in stati.keys():
            if status in self:
                r.append(stati[status])
        return tuple(r)


def get_user_order(
    event_slug: str, user: User, status: PretixPaymentStatus
) -> PretixOrder | None:
    if not PretixSettings(db_session=db.session).pretix_enabled:
        return None

    request_path = urljoin(
        PRETIX_BASE_URL, f"events/{event_slug}/via_users/{user.id}/orders/"
    )
    args = {"status__in": ",".join(status.strings())}
    resp = PRETIX_SESSION.get(request_path, params=args)
    if not resp.ok:
        return None

    for order in resp.json():
        return PretixOrder(code=order["code"], secret=order["secret"])
    return None


def get_user_orders(
    user: User, status: PretixPaymentStatus
) -> dict[str, list[PretixOrder]]:
    if not PretixSettings(db_session=db.session).pretix_enabled:
        return {}

    request_path = urljoin(PRETIX_BASE_URL, f"via_users/{user.id}/orders/")
    args = {"status__in": ",".join(status.strings())}
    try:
        resp = PRETIX_SESSION.get(request_path, params=args)
        resp.raise_for_status()
    except (requests.exceptions.ConnectionError, requests.exceptions.HTTPError):
        _logger.debug("Could not request user pretix events", exc_info=True)
        return {}

    if not resp.ok:
        return {}

    events: dict[str, list[PretixOrder]] = defaultdict(list)
    for order in resp.json():
        events[order["event"]["slug"]].append(
            PretixOrder(code=order["code"], secret=order["secret"])
        )
    return events


def get_pretix_user_exchange_key():
    key = app.secret_key
    while len(key) < 44:
        key += key

    # Add "=" as base64 padding to make the base64 encoding 32 bytes.
    return key[:43] + "="


def get_fernet_token_from_user(user: User) -> str:
    key = get_pretix_user_exchange_key()
    return fernet.Fernet(key).encrypt(str(user.id).encode()).decode("utf-8")


def get_user_from_pretix_link(fernet_token: str) -> User | None:
    key = get_pretix_user_exchange_key()
    try:
        user_id = int(
            fernet.Fernet(key)
            .decrypt(fernet_token.encode(), ttl=int(timedelta(days=1).total_seconds()))
            .decode()
        )

        return user_service.find_by_id(user_id)
    except fernet.InvalidToken:
        return None


def get_event_url(event_slug: str) -> str:
    pretix_settings = PretixSettings(db_session=db.session)
    return os.path.join(
        pretix_settings.pretix_host,
        pretix_settings.pretix_organizer,
        event_slug,
    )


PretixUserState = Literal[
    "login", "testmode", "favourer_disallowed", "membership", "register"
]


def user_widget_state(event: PretixEvent, user: User) -> PretixUserState:
    if "pretix_via" not in event.details["plugins"]:
        return "register"

    if not user.is_authenticated:
        return "login"

    if event.details["testmode"]:
        return "testmode"

    if event.settings.get("via_membership_required", True):
        if user.favourer and not event.settings.get("via_favourer_allowed", True):
            return "favourer_disallowed"
        if not user.has_paid and not user.favourer:
            return "membership"
    return "register"
