from sqlalchemy import func, select
from sqlalchemy.orm import Session

from app import db
from app.models.setting_model import Setting


def create_setting() -> Setting:
    return Setting()


def save(setting: Setting) -> Setting:
    db.session.add(setting)
    db.session.commit()
    return setting


def find_by_key(key) -> Setting | None:
    return db.session.query(Setting).filter_by(key=key).first()


def delete_setting(setting: Setting):
    db.session.delete(setting)
    db.session.commit()


def find_by_keys_case_insensitive(db_session: Session, keys: set[str]) -> list[Setting]:
    keys = {k.lower() for k in keys}
    stmt = select(Setting).where(func.lower(Setting.key).in_(keys))
    return db_session.execute(stmt).scalars().all()
