from sqlalchemy import select
from sqlalchemy.orm import joinedload

from app import db
from app.api.schema import MultilangStringDict, PageSearchParameters
from app.models.committee import Committee, CommitteeTag
from app.models.page import PageRevision
from app.repository.utils.pagination import search_columns
from app.service import page_service
from app.utils.pagination import Pagination


def find_committee_by_page_id(page_id: int) -> Committee | None:
    return db.session.query(Committee).filter_by(page_id=page_id).one_or_none()


def create(
    name: MultilangStringDict,
    coordinator_id: int,
    group_id: int,
    page_id: int,
    coordinator_interim: bool,
    open_new_members: bool,
    tags: list[dict],
    pressure: int,
    description: MultilangStringDict,
):
    committee = Committee(
        en_name=name["en"],
        nl_name=name["nl"],
        coordinator_id=coordinator_id,
        group_id=group_id,
        page_id=page_id,
        coordinator_interim=coordinator_interim,
        open_new_members=open_new_members,
        tags=(
            db.session.query(CommitteeTag)
            .filter(CommitteeTag.id.in_(map(lambda i: i["id"], tags)))
            .all()
        ),
        pressure=pressure,
        en_description=description["en"],
        nl_description=description["nl"],
    )
    db.session.add(committee)
    db.session.commit()
    return committee


def edit(
    committee: Committee,
    name: MultilangStringDict,
    coordinator_id: int,
    group_id: int,
    page_id: int,
    coordinator_interim: bool,
    open_new_members: bool,
    tags: list[dict],
    pressure: int,
    description: MultilangStringDict,
):
    committee.en_name = name["en"]
    committee.nl_name = name["nl"]
    committee.coordinator_id = coordinator_id
    committee.group_id = group_id
    committee.page_id = page_id
    committee.coordinator_interim = coordinator_interim
    committee.open_new_members = open_new_members
    committee.tags = (
        db.session.query(CommitteeTag)
        .filter(CommitteeTag.id.in_(map(lambda i: i["id"], tags)))
        .all()
    )
    committee.pressure = pressure
    committee.en_description = description["en"]
    committee.nl_description = description["nl"]
    db.session.add(committee)
    db.session.commit()


def set_committee_picture(committee: Committee, file_id: int):
    committee.picture_file_id = file_id
    db.session.add(committee)
    db.session.commit()


def get_all_committees_with_page_revisions() -> list[tuple[Committee, PageRevision]]:
    committees = db.session.query(Committee).options(joinedload("page")).all()
    page_revisions = page_service.get_latest_revisions([c.page for c in committees])
    return list(zip(committees, page_revisions))


def get_all_committees() -> list[Committee]:
    return (
        db.session.query(Committee)
        .options(joinedload(Committee.group))
        .options(joinedload(Committee.page))
        .all()
    )


def paginated_search_all_committees(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(Committee).order_by(Committee.id.asc())

    q = q.options(joinedload(Committee.page))
    q = q.options(joinedload(Committee.group))

    q = search_columns(q, pagination.search, Committee.nl_name, Committee.en_name)

    return q.paginate(pagination.page, pagination.limit, error_out=False)


def delete_committee(committee):
    db.session.delete(committee)
    db.session.commit()


def get_all_tags() -> list[CommitteeTag]:
    stmt = select(CommitteeTag).order_by(CommitteeTag.id)
    return db.session.execute(stmt).scalars().all()
