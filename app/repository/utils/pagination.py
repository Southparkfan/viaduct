from flask_sqlalchemy import BaseQuery
from sqlalchemy import Column
from sqlalchemy.sql.operators import ColumnOperators

from app import db


def search_columns(
    query: BaseQuery,
    search: str,
    *searchable_columns: Column | ColumnOperators,
) -> BaseQuery:
    if search == "":
        return query

    filters = []
    for column in searchable_columns:
        filters.append(column.ilike(f"%{search}%"))
    return query.filter(db.or_(*filters))


def sort_columns(
    query: BaseQuery,
    sort: str,
    sorting_columns: dict[str, Column | ColumnOperators],
) -> BaseQuery:
    if not sort:
        return query

    ascending = True
    if sort.startswith("-"):
        ascending = False
        sort = sort[1:]

    if sort not in sorting_columns:
        return query

    column = sorting_columns[sort]
    if not ascending:
        column = column.desc()
    return query.order_by(column)
