from datetime import datetime, timedelta, timezone

from flask_sqlalchemy import Pagination
from sqlalchemy import asc, func, nullsfirst, or_, select, update
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.engine import Result
from sqlalchemy.orm import Session, raiseload

from app import db
from app.api.schema import PageSearchParameters
from app.models.education import Education
from app.models.user import User, UserEducation, UserEducationStatus
from app.repository.utils.pagination import search_columns


def create_user():
    u = User()
    db.session.add(u)
    return u


def save(user):
    db.session.add(user)
    db.session.commit()


def save_all(users):
    db.session.add_all(users)
    db.session.commit()


def find_members():
    return (
        db.session.query(User)
        .filter_by(has_paid=True)
        .order_by(User.first_name)
        .options(raiseload("*"))
        .all()
    )


def find_members_of_merit(session) -> Result:
    stmt = (
        select(
            User.id,
            User.first_name,
            User.last_name,
            func.concat(User.first_name, " ", User.last_name).label("name"),
            User.member_of_merit_date,
        )
        .where(User.member_of_merit_date.isnot(None))
        .order_by(User.member_of_merit_date)
    )
    return session.execute(stmt)


def find_all_user_ids() -> list[int]:
    r = []
    for (value,) in db.session.query(User.id).distinct():
        r.append(value)
    return r


def find_by_id(user_id):
    return db.session.query(User).filter_by(id=user_id).one_or_none()


def find_by_ids(user_ids: list[int]):
    return db.session.query(User).filter(User.id.in_(user_ids)).all()


def find_user_by_email(email):
    return db.session.query(User).filter_by(email=email).one_or_none()


def find_user_by_copernica_id(copernica_id: int) -> User:
    return db.session.query(User).filter_by(copernica_id=copernica_id).one_or_none()


def paginated_search_all_users(pagination: PageSearchParameters) -> Pagination:
    q = db.session.query(User).order_by(User.id.asc())

    q = search_columns(
        q,
        pagination.search,
        User.email,
        func.concat(User.first_name, " ", User.last_name),
        User.student_id,
    )

    return q.paginate(pagination.page, pagination.limit, False)


def find_user_by_student_id(student_id, needs_confirmed=True):
    def query_user(confirm_status):
        return (
            db.session.query(User)
            .filter_by(student_id=student_id, student_id_confirmed=confirm_status)
            .first()
        )

    confirmed_user = query_user(True)

    # If we allow unconfirmed and did not find a confirmed user,
    # try to find an unconfirmed user
    if not needs_confirmed and not confirmed_user:
        return query_user(False)

    return confirmed_user


def find_all_users_with_unconfirmed_student_id(student_id):
    return (
        db.session.query(User)
        .filter_by(student_id=student_id, student_id_confirmed=False)
        .all()
    )


def find_active_mailing_users_with_copernica_id(copernica_ids: list[int]):
    return (
        db.session.query(User)
        .filter(User.copernica_id.in_(copernica_ids))
        .filter(User.mailinglists.any())
        .all()
    )


def get_user_ids_like(potential_users: list[str]) -> list[tuple[int | None, str]]:
    """Gets user.ids based on a list of full names."""

    if not potential_users:
        return []

    user_expr = func.concat(User.first_name, " ", User.last_name)

    sub = db.session.query(func.unnest(potential_users).label("name")).subquery()
    return (
        db.session.query(func.min(User.id), sub.c.name)
        .join(User, user_expr.ilike(sub.c.name), isouter=True)
        .group_by(sub.c.name)
        .all()
    )


def set_user_education(db_session: Session, user: User, educations: list[Education]):
    """
    Update the user educations in the database.

    This is done by setting the last_seen date and creating new
    """

    # The user might be not have an ID, if it is newly created.
    db_session.flush()

    current_education = user.educations

    removed_educations = set(current_education) - set(educations)
    new_educations = set(educations) - set(current_education)

    set_last_seen_stmt = (
        update(UserEducation)
        .values({UserEducation.last_seen: func.now()})
        .where(
            UserEducation.user_id == user.id,
            UserEducation.education_id.in_([ed.id for ed in removed_educations]),
            UserEducation.last_seen.is_(None),
        )
    )
    db_session.execute(set_last_seen_stmt)

    for education in new_educations:
        ue = UserEducation()
        ue.user_id = user.id
        ue.education_id = education.id
        ue.last_seen = None

        db_session.add(ue)

    if user.student_id_confirmed:
        status_stmt = (
            insert(UserEducationStatus)
            .values(
                {
                    UserEducationStatus.user_id: user.id,
                    UserEducationStatus.last_checked: datetime.now(timezone.utc),
                }
            )
            .on_conflict_do_update(
                index_elements=["user_id"],
                set_={UserEducationStatus.last_checked: datetime.now(timezone.utc)},
            )
        )
        db_session.execute(status_stmt)

    # TODO we expect other places to commit in the future!
    # db_session.commit()


def get_users_with_educations_not_updated_for(
    db_session: Session, num: int, interval: timedelta
):
    """
    Get a subset of users of which their educations have been updated least recently.

    :param db_session: active database session to query with
    :param num: max number of users to return
    :param interval: users for which educations have not recently been updated.
    :return:
    """
    min_modified = datetime.now() - interval
    stmt = (
        select(User)
        # Some users might not be present in the status, select NULLs.
        .join(UserEducationStatus, isouter=True)
        .where(
            or_(
                UserEducationStatus.last_checked < min_modified,
                # Select NULLs for non-existing UserEducationStatus
                UserEducationStatus.last_checked.is_(None),
            ),
            # We do not mess with user supplied educations.
            User.student_id_confirmed.is_(True),
        )
        .order_by(nullsfirst(asc(UserEducationStatus.last_checked)), User.id)
        .limit(num)
    )
    return db_session.execute(stmt).unique().scalars().all()
