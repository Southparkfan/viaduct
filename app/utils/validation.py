import re

from wtforms.validators import HostnameValidation

email_regex = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*\Z"
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\'
    r'[\001-\011\013\014\016-\177])*"\Z)',
    # quoted-string
    re.IGNORECASE,
)

validate_hostname = HostnameValidation(require_tld=True)


def validate_email_address(value: str) -> bool:
    """
    Validate that the given address is a good email address.

    The validation is taken from the wtforms.validators.Email validator.
    :param value: an email address.
    :return: true if valid, false otherwise.
    """
    if not value or "@" not in value:
        return False

    user_part, domain_part = value.rsplit("@", 1)

    if not email_regex.match(user_part):
        return False

    if not validate_hostname(domain_part):
        return False

    return True
