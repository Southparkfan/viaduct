import datetime
import random
from typing import Any

from flask import Response, request, url_for
from flask_login import current_user
from flask_wtf.csrf import generate_csrf

from app import DatabaseSettingsMixin, app, db, get_locale, version
from app.roles import Roles


class SentryPublicSettings(DatabaseSettingsMixin):
    sentry_dsn_frontend = "https://d20fbd1634454649bd8877942ebb5657@sentry.io/1285048"
    environment = "Development"


@app.context_processor
def inject_urls():
    return dict(
        request_path=request.path,
        request_base_url=request.base_url,
        request_url=request.url,
        request_url_root=request.url_root,
    )


@app.context_processor
def inject_seo_write_permission():
    from app.service import role_service

    can_write_seo = role_service.user_has_role(current_user, Roles.SEO_WRITE)
    return dict(can_write_seo=can_write_seo)


@app.context_processor
def inject_privacy_policy_url():
    url = url_for("home.privacy_policy", lang=get_locale())
    return dict(privacy_policy_url=url)


@app.context_processor
def inject_date():
    return dict(current_date=datetime.datetime.now(datetime.timezone.utc))


@app.context_processor
def inject_viaduct_javascript_values():
    return dict(
        locale=get_locale(),
        recaptcha_public_key=app.config["RECAPTCHA_PUBLIC_KEY"],
        csrf_token=generate_csrf(),
    )


@app.context_processor
def inject_sentry_details() -> dict[str, Any]:
    settings = SentryPublicSettings(db_session=db.session)
    return dict(
        app_version=version.hash,
        sentry_dsn_frontend=settings.sentry_dsn_frontend,
        sentry_environment=settings.environment,
    )


@app.context_processor
def inject_sentry_dsn_frontend():
    return dict()


@app.context_processor
def inject_navigation():
    """
    Inject variables needed to render the navigations.

    Files using these variables:
    app/templates/navigation/view_bar.htm
    app/templates/navigation/view_sidebar.htm
    app/templates/navigation/view_backtrack.htm
    """
    from app.forms.user import SignInForm
    from app.service import navigation_service

    login_form = SignInForm()

    top_entries = navigation_service.get_navigation_top_entries()
    top_entries = navigation_service.remove_unauthorized(top_entries, current_user)

    backtrack = navigation_service.get_navigation_backtrack(request.path)

    side_entries, current_entry = navigation_service.get_navigation_side_entries(
        request.path, current_user
    )
    side_entries = navigation_service.remove_unauthorized(side_entries, current_user)

    return dict(
        navigation_bar_entries=top_entries,
        navigation_side_entries=side_entries,
        navigation_current_entry=current_entry,
        navigation_get_children=navigation_service.get_children,
        login_form=login_form,
        backtrack=backtrack,
    )


@app.context_processor
def inject_carousel():
    from app.service import company_service

    banners = company_service.find_all_banners()

    # Explicitly shuffle the results, so they are never the same in client.
    random.shuffle(banners)
    return dict(carousel_items=banners)


@app.context_processor
def inject_viewmodels():
    from app.views.viewmodel.banner import BannerViewModel
    from app.views.viewmodel.dropdown import NavigationBarViewModel
    from app.views.viewmodel.membership import MembershipBannerViewModel

    return dict(
        banner_view=BannerViewModel(),
        navigation_view=NavigationBarViewModel(),
        membership_view=MembershipBannerViewModel(),
    )


@app.after_request
def set_auth_cookie(response: Response) -> Response:
    from app.service import oauth_service

    if current_user.is_authenticated:
        token = oauth_service.get_manual_token(current_user.id)
        response.set_cookie("access_token", token.access_token)
    return response
