import re

from markdown.blockprocessors import BlockQuoteProcessor
from markdown.extensions import Extension


class MarkdownBlockquotesExtension(Extension):
    def extendMarkdown(self, md):
        md.registerExtension(self)
        md.parser.blockprocessors.register(
            EncodedBlockQuoteProcessor(md.parser), "encoded_quote", 20
        )


class EncodedBlockQuoteProcessor(BlockQuoteProcessor):
    #  This overrides the original regex where a normal '>' is used. This is
    #  necessary because we first clean the user input, which encodes these
    #  signs.
    RE = re.compile(r"(^|\n)[ ]{0,3}&gt;[ ]?(.*)")
