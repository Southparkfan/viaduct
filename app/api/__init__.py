from flask import Blueprint

from app import app
from app.api.activity import (
    ActivityListResource,
    ActivityPictureResource,
    ActivityResource,
)
from app.api.alv import (
    AlvDocumentListResource,
    AlvDocumentResource,
    AlvListResource,
    AlvMinuteResource,
    AlvParsedMinuteResource,
    AlvParseMinuteResource,
    AlvResource,
)
from app.api.bug import BugReportView
from app.api.challenge import (
    ChallengeAdminSubmissionResource,
    ChallengeListResource,
    ChallengeRankingResource,
    ChallengeResource,
    ChallengeSubmissionResource,
    UserChallengeListResource,
)
from app.api.committee.committee import (
    CommitteeListResource,
    CommitteePictureResource,
    CommitteeResource,
    CommitteeTagsResource,
)
from app.api.company.company import (
    CompanyListResource,
    CompanyPublicResource,
    CompanyResource,
)
from app.api.company.company_banner import BannerResource, CompanyBannerResource
from app.api.company.company_job import (
    CompanyJobListResource,
    JobContractsOfServices,
    JobListResource,
    JobResource,
)
from app.api.company.company_logo import CompanyLogoResource
from app.api.company.company_profile import (
    CompanyProfileIdResource,
    CompanyProfileListResource,
    CompanyProfileSlugResource,
)
from app.api.declaration.declaration import (
    DeclarationResource,
    DeclarationUploadResource,
)
from app.api.domjudge import (
    DOMjudgeBannerResource,
    DOMjudgeContestListResource,
    DOMjudgeContestResource,
    DOMjudgeContestUserListResource,
)
from app.api.education.lectures import LecturesMailingView
from app.api.examination.course import (
    CourseListResource,
    CourseResource,
    UserCoursesResource,
)
from app.api.examination.education import (
    EducationListResource,
    EducationResource,
    UserEducationsResource,
)
from app.api.examination.examination import (
    CourseExaminationListResource,
    ExaminationAnswerUploadResource,
    ExaminationExamUploadResource,
    ExaminationListResource,
    ExaminationResource,
    ExaminationSummaryUploadResource,
)
from app.api.group.google import GoogleGroupResource
from app.api.group.group import GroupListResource, GroupResource
from app.api.group.roles import GroupRoleSchema
from app.api.group.users import GroupUserListResource
from app.api.health import HealthCheckResource
from app.api.mailinglist.mailinglist import MailingListListResource, MailingListResource
from app.api.meeting import MeetingListResource, MeetingResource
from app.api.navigation.navigation import (
    NavigationListResource,
    NavigationOrderResource,
    NavigationResource,
)
from app.api.news import NewsListResource, NewsResource
from app.api.newsletter import (
    NewsletterInitialResource,
    NewsletterListResource,
    NewsletterResource,
)
from app.api.page.page import (
    PageListResource,
    PageRenderPreviewResource,
    PageRenderResource,
    PageResource,
    PageRevisionResource,
)
from app.api.photos.album import AlbumListResource, AlbumResource, RandomPhotoResource
from app.api.pimpy.minutes import (
    GroupMinuteResource,
    MinuteListResource,
    MinuteResource,
)
from app.api.pimpy.tasks import (
    GroupTaskListResource,
    TaskListResource,
    TaskResource,
    UserTaskListResource,
)
from app.api.pretix.pretix import PretixUserCheck, PretixUserEvents
from app.api.redirect.redirect import RedirectListResource, RedirectResource
from app.api.schema import SchemaResource
from app.api.search.search import SearchIndexResource, SearchResource
from app.api.task import (
    AsyncCopernicaSync,
    AsyncDatanoseSync,
    AsyncTaskResource,
    AsyncTasksResource,
)
from app.api.tutoring.tutors import (
    TutorCourseResource,
    TutorCoursesResource,
    TutoringResource,
    TutorsResource,
    TutorTutoringAcceptResource,
    TutorTutoringListResource,
    TutorTutoringResource,
)
from app.api.user.applications import (
    UserApplicationResource,
    UserRevokeApplicationResource,
)
from app.api.user.avatar import UserAvatarResource
from app.api.user.declaration import UserDeclarationsResource
from app.api.user.groups import UserGroupDetailsResource, UserGroupsResource
from app.api.user.mailinglist_subscriptions import UserMailingListSubscriptionResource
from app.api.user.merit import MeritUserListResource
from app.api.user.role import UserRoleResource
from app.api.user.tfa import UserTfaResource
from app.api.user.tutoring import UserTutoringListResource, UserTutoringResource
from app.api.user.user import UserListResource, UserResource

app.add_url_rule(
    "/api/lectures/mailinglist/",
    view_func=LecturesMailingView.as_view("api.lectures.mailinglist"),
)
blueprint = Blueprint("api", __name__, url_prefix="/api")
blueprint.add_url_rule(
    "/_schema/<string:schema_name>/",
    view_func=SchemaResource.as_view("schema"),
    endpoint="schema",
)
app.add_url_rule("/api/bugs/", view_func=BugReportView.as_view("api.bugs.report"))

app.add_url_rule(
    "/api/health/",
    view_func=HealthCheckResource.as_view("api.health"),
)
# Pimpy Tasks
app.add_url_rule("/api/tasks/", view_func=TaskListResource.as_view("api.tasks"))
app.add_url_rule(
    "/api/tasks/<string:task_id>/",
    view_func=TaskResource.as_view("api.task"),
)
app.add_url_rule(
    "/api/groups/<group:group>/tasks/",
    view_func=GroupTaskListResource.as_view("api.group.tasks"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/tasks/",
    view_func=UserTaskListResource.as_view("api.user.tasks"),
)

# Pimpy Minutes
app.add_url_rule(
    "/api/minutes/",
    view_func=MinuteListResource.as_view("api.minutes"),
)
app.add_url_rule(
    "/api/minutes/<minute:minute>/",
    view_func=MinuteResource.as_view("api.minute"),
)
app.add_url_rule(
    "/api/groups/<group:group>/minutes/",
    view_func=GroupMinuteResource.as_view("api.group.minutes"),
)

# Meeting
app.add_url_rule(
    "/api/meetings/<meeting:meeting>/",
    view_func=MeetingResource.as_view("api.meeting"),
)
app.add_url_rule(
    "/api/meetings/",
    view_func=MeetingListResource.as_view("api.meetings"),
)

# Alv
app.add_url_rule(
    "/api/alvs/<alv:alv>/",
    view_func=AlvResource.as_view("api.alv"),
)
app.add_url_rule(
    "/api/alvs/",
    view_func=AlvListResource.as_view("api.alvs"),
)
app.add_url_rule(
    "/api/alvs/parse_minutes/",
    view_func=AlvParseMinuteResource.as_view("api.alv.parse_minutes"),
)
app.add_url_rule(
    "/api/alvs/<alv:alv>/parsed_minutes/",
    view_func=AlvParsedMinuteResource.as_view("api.alv.parsed_minutes"),
)
app.add_url_rule(
    "/api/alvs/document/<int:doc_id>/",
    view_func=AlvDocumentResource.as_view("api.alv.document"),
)
app.add_url_rule(
    "/api/alvs/<alv:alv>/minutes/",
    view_func=AlvMinuteResource.as_view("api.alv.minutes"),
)
app.add_url_rule(
    "/api/alvs/<alv:alv>/documents/",
    view_func=AlvDocumentListResource.as_view("api.alv.documents"),
)

# Activity
app.add_url_rule(
    "/api/activities/<activity:activity>/",
    view_func=ActivityResource.as_view("api.activity"),
)
app.add_url_rule(
    "/api/activities/",
    view_func=ActivityListResource.as_view("api.activities"),
)
app.add_url_rule(
    "/api/activities/<activity:activity>/picture/",
    view_func=ActivityPictureResource.as_view("api.activities.picture"),
)

# User
app.add_url_rule(
    "/api/users/",
    view_func=UserListResource.as_view("api.users"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/",
    view_func=UserResource.as_view("api.user"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/groups/",
    view_func=UserGroupsResource.as_view("api.user.groups"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/groups/details/",
    view_func=UserGroupDetailsResource.as_view("api.user.groups_details"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/roles/",
    view_func=UserRoleResource.as_view("api.user.roles"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/applications/",
    view_func=UserApplicationResource.as_view("api.user.applications"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/applications/<string:client_id>/revoke/",
    view_func=UserRevokeApplicationResource.as_view("api.user.applications.revoke"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/tutoring/",
    view_func=UserTutoringListResource.as_view("api.user.tutorings"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/tutoring/<tutoring:tutoring>/",
    view_func=UserTutoringResource.as_view("api.user.tutoring"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/declarations/",
    view_func=UserDeclarationsResource.as_view("api.user.declarations"),
)

app.add_url_rule(
    "/api/users/merit/",
    view_func=MeritUserListResource.as_view("api.user.merit"),
)

# Group
app.add_url_rule(
    "/api/groups/",
    view_func=GroupListResource.as_view("api.groups"),
)
app.add_url_rule(
    "/api/groups/<group:group>/",
    view_func=GroupResource.as_view("api.group"),
)
app.add_url_rule(
    "/api/groups/<group:group>/users/",
    view_func=GroupUserListResource.as_view("api.group.users"),
)
app.add_url_rule(
    "/api/groups/<group:group>/roles/",
    view_func=GroupRoleSchema.as_view("api.group.roles"),
)
app.add_url_rule(
    "/api/groups/<int:group_id>/google/",
    view_func=GoogleGroupResource.as_view("api.group.google"),
)

# Courses
app.add_url_rule(
    "/api/courses/",
    view_func=CourseListResource.as_view("api.courses"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/courses/",
    view_func=UserCoursesResource.as_view("api.user.courses"),
)
app.add_url_rule(
    "/api/courses/<course:course>/",
    view_func=CourseResource.as_view("api.course"),
)

# Education
app.add_url_rule(
    "/api/educations/",
    view_func=EducationListResource.as_view("api.educations"),
)
app.add_url_rule(
    "/api/users/<user_self:user>/educations/",
    view_func=UserEducationsResource.as_view("api.user.educations"),
)
app.add_url_rule(
    "/api/educations/<int:education_id>/",
    view_func=EducationResource.as_view("api.education"),
)

# Examination
app.add_url_rule(
    "/api/examinations/",
    view_func=ExaminationListResource.as_view("api.examinations"),
)
app.add_url_rule(
    "/api/examinations/<examination:examination>/",
    view_func=ExaminationResource.as_view("api.examination"),
)
app.add_url_rule(
    "/api/examinations/<examination:examination>/exam/",
    view_func=ExaminationExamUploadResource.as_view("api.examination.exam"),
)
app.add_url_rule(
    "/api/examinations/<examination:examination>/answers/",
    view_func=ExaminationAnswerUploadResource.as_view("api.examination.answers"),
)
app.add_url_rule(
    "/api/examinations/<examination:examination>/summary/",
    view_func=ExaminationSummaryUploadResource.as_view("api.examination.summary"),
)
app.add_url_rule(
    "/api/courses/<course:course>/examinations/",
    view_func=CourseExaminationListResource.as_view("api.course.examinations"),
)

# Tutoring
app.add_url_rule(
    "/api/tutors/",
    view_func=TutorsResource.as_view("api.tutors"),
)
app.add_url_rule(
    "/api/tutorings/",
    view_func=TutoringResource.as_view("api.tutorings"),
)
app.add_url_rule(
    "/api/tutors/<user_self:user>/tutorings/",
    view_func=TutorTutoringListResource.as_view("api.tutors.tutorings"),
)
app.add_url_rule(
    "/api/tutors/<user_self:user>/courses/",
    view_func=TutorCoursesResource.as_view("api.tutor.courses"),
)
app.add_url_rule(
    "/api/tutors/<user_self:user>/courses/<tutor:tutor>/",
    view_func=TutorCourseResource.as_view("api.tutor.course"),
)
app.add_url_rule(
    "/api/tutors/<user_self:user>/tutorings/<tutoring:tutoring>/",
    view_func=TutorTutoringResource.as_view("api.tutors.tutoring"),
)
app.add_url_rule(
    "/api/tutors/<user_self:user>/tutorings/<tutoring:tutoring>/accept/",
    view_func=TutorTutoringAcceptResource.as_view("api.tutors.tutoring.accept"),
)
# Company
app.add_url_rule(
    "/api/companies/<company:company>/",
    view_func=CompanyResource.as_view("api.company"),
)

app.add_url_rule(
    "/api/companies/<company:company>/public/",
    view_func=CompanyPublicResource.as_view("api.company.public"),
)
app.add_url_rule(
    "/api/companies/<company:company>/logo/",
    view_func=CompanyLogoResource.as_view("api.company_logo"),
)
app.add_url_rule(
    "/api/companies/<company:company>/banner/",
    view_func=CompanyBannerResource.as_view("api.company_banner"),
)
app.add_url_rule(
    "/api/companies/<company:company>/profile/",
    view_func=CompanyProfileIdResource.as_view("api.company_profile.id"),
)
app.add_url_rule(
    "/api/companies/<company_path:company>/profile/",
    view_func=CompanyProfileSlugResource.as_view("api.company_profile.slug"),
)
app.add_url_rule(
    "/api/companies/",
    view_func=CompanyListResource.as_view("api.companies"),
)

# Company jobs
app.add_url_rule(
    "/api/jobs/",
    view_func=JobListResource.as_view("api.jobs"),
)
app.add_url_rule(
    "/api/jobs/types/",
    view_func=JobContractsOfServices.as_view("api.jobs.types"),
)
app.add_url_rule(
    "/api/jobs/<int:job_id>/",
    view_func=JobResource.as_view("api.job"),
)
app.add_url_rule(
    "/api/companies/<company:company>/jobs/",
    view_func=CompanyJobListResource.as_view("api.company.jobs"),
)
app.add_url_rule(
    "/api/companies/<company_path:company>/jobs/",
    view_func=CompanyJobListResource.as_view("api.company.jobs.slug"),
)

# Banner (for footer)
app.add_url_rule(
    "/api/banners/",
    view_func=BannerResource.as_view("api.banners"),
)

# Company pages
app.add_url_rule(
    "/api/profiles/",
    view_func=CompanyProfileListResource.as_view("api.companies.profilelist"),
)  # Search
app.add_url_rule(
    "/api/search/",
    view_func=SearchResource.as_view("api.search"),
)
app.add_url_rule(
    "/api/search/index/",
    view_func=SearchIndexResource.as_view("api.search.index"),
)
app.add_url_rule(
    "/api/users/<int:user_id>/avatar/",
    view_func=UserAvatarResource.as_view("api.user.avatar"),
)

# User tfa
app.add_url_rule(
    "/api/users/self/tfa/",
    view_func=UserTfaResource.as_view("api.user.tfa"),
)

# User role
app.add_url_rule(
    "/api/users/self/role/",
    view_func=UserRoleResource.as_view("api.user.role"),
)

# User mailinglist subscriptions
app.add_url_rule(
    "/api/users/<user_self:user>/mailinglist-subscriptions/",
    view_func=UserMailingListSubscriptionResource.as_view(
        "api.user.mailinglist_subscriptions"
    ),
)

# Photos
app.add_url_rule(
    "/api/photos/",
    view_func=AlbumListResource.as_view("api.albums"),
)
app.add_url_rule(
    "/api/photos/<int:album_id>/",
    view_func=AlbumResource.as_view("api.album"),
)
app.add_url_rule(
    "/api/photos/random/",
    view_func=RandomPhotoResource.as_view("api.photo"),
)

# Pages
app.add_url_rule(
    "/api/pages/",
    view_func=PageListResource.as_view("api.pages"),
)
app.add_url_rule(
    "/api/pages/<page:page>/",
    view_func=PageResource.as_view("api.page"),
)
app.add_url_rule(
    "/api/pages/<page:page>/rev/",
    view_func=PageRevisionResource.as_view("api.page_revision.new"),
)

app.add_url_rule(
    "/api/pages/<page:page>/rev/<int:revision_id>/",
    view_func=PageRevisionResource.as_view("api.page_revision"),
)
app.add_url_rule(
    "/api/pages/<page:page>/rev/latest/",
    view_func=PageRevisionResource.as_view("api.page_revision.latest"),
)
app.add_url_rule(
    "/api/pages/render/preview/",
    view_func=PageRenderPreviewResource.as_view("api.page_render.preview"),
)
app.add_url_rule(
    "/api/pages/render/<string:lang>/<path:path>/",
    view_func=PageRenderResource.as_view("api.page_render"),
)

# Pretix
app.add_url_rule(
    "/api/pretix/users/<string:fernet_token>/",
    view_func=PretixUserCheck.as_view("api.pretix.user"),
)
app.add_url_rule(
    "/api/pretix/events/",
    view_func=PretixUserEvents.as_view("api.pretix.events"),
)

# MailingList
app.add_url_rule(
    "/api/mailinglists/<int:mailinglist_id>/",
    view_func=MailingListResource.as_view("api.mailinglist"),
)
app.add_url_rule(
    "/api/mailinglists/",
    view_func=MailingListListResource.as_view("api.mailinglists"),
)

# Navigation
app.add_url_rule(
    "/api/navigation/ordering/",
    view_func=NavigationOrderResource.as_view("api.navigation.order"),
)
app.add_url_rule(
    "/api/navigation/<navigation_entry:entry>/",
    view_func=NavigationResource.as_view("api.navigation"),
)
app.add_url_rule(
    "/api/navigation/",
    view_func=NavigationListResource.as_view("api.navigations"),
)

# Challenge
app.add_url_rule(
    "/api/challenges/ranking/",
    view_func=ChallengeRankingResource.as_view("api.challenges.ranking"),
)
app.add_url_rule(
    "/api/challenges/users/self/",
    view_func=UserChallengeListResource.as_view("api.challenges.user"),
)
app.add_url_rule(
    "/api/challenges/<challenge:challenge>/",
    view_func=ChallengeResource.as_view("api.challenge"),
)
app.add_url_rule(
    "/api/challenges/",
    view_func=ChallengeListResource.as_view("api.challenges"),
)
app.add_url_rule(
    "/api/challenges/<challenge:challenge>/submission/",
    view_func=ChallengeSubmissionResource.as_view("api.challenges.submission"),
)
app.add_url_rule(
    "/api/challenges/<challenge:challenge>/submission/admin/",
    view_func=ChallengeAdminSubmissionResource.as_view(
        "api.challenge_submission_admin"
    ),
)

# News
app.add_url_rule(
    "/api/news/",
    view_func=NewsListResource.as_view("api.news"),
)
app.add_url_rule(
    "/api/news/<news:news>/",
    view_func=NewsResource.as_view("api.news_item"),
)
app.add_url_rule(
    "/api/newsletters/initial/",
    view_func=NewsletterInitialResource.as_view("api.newsletter.initial"),
)
app.add_url_rule(
    "/api/newsletters/<newsletter:newsletter>/",
    view_func=NewsletterResource.as_view("api.newsletter"),
)
app.add_url_rule(
    "/api/newsletters/",
    view_func=NewsletterListResource.as_view("api.newsletters"),
)

# Declaration
app.add_url_rule(
    "/api/declaration/upload/",
    view_func=DeclarationUploadResource.as_view("api.declaration.upload"),
)
app.add_url_rule(
    "/api/declaration/send/",
    view_func=DeclarationResource.as_view("api.declaration.send"),
)

app.add_url_rule(
    "/api/committees/",
    view_func=CommitteeListResource.as_view("api.committees"),
)
app.add_url_rule(
    "/api/committees/<committee:committee>/",
    view_func=CommitteeResource.as_view("api.committee"),
)
app.add_url_rule(
    "/api/committees/<committee:committee>/picture/",
    view_func=CommitteePictureResource.as_view("api.committees.picture"),
)
app.add_url_rule(
    "/api/committees/tags/",
    view_func=CommitteeTagsResource.as_view("api.committees.tags"),
)

# Redirects
app.add_url_rule(
    "/api/redirect/",
    view_func=RedirectListResource.as_view("api.redirects"),
)
app.add_url_rule(
    "/api/redirect/<redirect:redirect>/",
    view_func=RedirectResource.as_view("api.redirect"),
)

# Async Tasks
app.add_url_rule(
    "/api/async/tasks/",
    view_func=AsyncTasksResource.as_view("api.async.tasks"),
)
app.add_url_rule(
    "/api/async/tasks/<string:task_id>/",
    view_func=AsyncTaskResource.as_view("api.async.task"),
)
app.add_url_rule(
    "/api/async/copernica/",
    view_func=AsyncCopernicaSync.as_view("api.async.copernica"),
)
app.add_url_rule(
    "/api/async/datanose/",
    view_func=AsyncDatanoseSync.as_view("api.async.datanose"),
)

# DOMjudge
app.add_url_rule(
    "/api/domjudge/contests/",
    view_func=DOMjudgeContestListResource.as_view("api.domjudge.contests"),
)
app.add_url_rule(
    "/api/domjudge/contests/<int:contest_id>/",
    view_func=DOMjudgeContestResource.as_view("api.domjudge.contest"),
)
app.add_url_rule(
    "/api/domjudge/contests/<int:contest_id>/banner/",
    view_func=DOMjudgeBannerResource.as_view("api.domjudge.contest.banner"),
)
app.add_url_rule(
    "/api/domjudge/contests/<int:contest_id>/users/",
    view_func=DOMjudgeContestUserListResource.as_view("api.domjudge.contest.users"),
)

app.register_blueprint(blueprint)
