import logging

from flask.views import MethodView
from marshmallow import fields

from app import worker
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.exceptions.base import ResourceNotFoundException, ValidationException
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.task import copernica, datanose

_logger = logging.getLogger(__name__)


class TaskRequestSchema(RestSchema):
    task = fields.String(required=True)


class AsyncTaskResource(MethodView):
    def get(self, task_id):
        t = worker.AsyncResult(task_id)
        return t._get_task_meta()


class AsyncTasksResource(MethodView):
    schema_post = TaskRequestSchema()

    @require_oauth()
    @require_role(Roles.TECHNICAL_ADMIN)
    @json_schema(schema_post)
    def post(self, request_body):
        task_name = request_body["task"]
        if not task_name.startswith("app"):
            raise ValidationException("Tasks must start with 'app'")

        try:
            # TODO Make this generic task start endpoint support arguments."""
            task = worker.tasks[task_name].delay()
            return task._get_task_meta()
        except KeyError:  # Task does not exist
            raise ResourceNotFoundException("task", task_name)
        except TypeError:  # Task arguments do not match
            raise ValidationException(f"Received invalid arguments for {task_name}")


class AsyncCopernicaSync(MethodView):
    @require_oauth(Scopes.user)
    @require_role(Roles.TECHNICAL_ADMIN)
    def post(self):
        task = copernica.update_all_users.delay()
        t = worker.AsyncResult(task.id)
        return dict(**t._get_task_meta(), task_id=task.id)


class AsyncDatanoseSync(MethodView):
    @require_oauth(Scopes.user)
    @require_role(Roles.TECHNICAL_ADMIN)
    def post(self):
        task = datanose.sync_datanose_courses.delay()
        t = worker.AsyncResult(task.id)
        return dict(**t._get_task_meta(), task_id=task.id)
