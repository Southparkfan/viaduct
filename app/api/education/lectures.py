from http import HTTPStatus

import pydantic
from flask import Response, request
from flask.views import MethodView
from pydantic import EmailStr

from app import db
from app.api.schema import RecaptchaStr, StudentIdStr, schema_registry
from app.exceptions.base import ValidationException
from app.service import copernica_service, user_service


class LecturesMailingView(MethodView):
    @schema_registry.register
    class LecturesMailingRequest(pydantic.BaseModel):
        first_name: str
        last_name: str
        email: EmailStr
        student_id: StudentIdStr
        recaptcha: RecaptchaStr

    def post(self) -> Response:
        try:
            req_json = request.get_json(force=True)
            data = self.LecturesMailingRequest.parse_obj(req_json)
            # We cannot do this check in the copernica_service.
            # It would create circular dependency with user_service.
            user = user_service.find_user_by_email(data.email)
            if user:
                raise ValidationException(
                    "E-mail is known as account, use mailing preferences in your "
                    "to sign up for lecture mailings."
                )
            user = user_service.find_user_by_student_id(
                data.student_id, needs_confirmed=True
            )
            if user:
                raise ValidationException(
                    "Student ID is known as account, use mailing preferences in your "
                    "account to sign up for lecture mailings."
                )
            copernica_service.create_lecture_only_profile(
                db_session=db.session,
                first_name=data.first_name,
                last_name=data.last_name,
                email=data.email,
                student_id=data.student_id,
            )
            return Response(status=HTTPStatus.CREATED)
        except pydantic.ValidationError as e:
            raise ValidationException(e.errors())
