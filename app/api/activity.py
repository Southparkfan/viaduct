from http import HTTPStatus

from flask import Response, request
from flask.views import MethodView
from flask_login import current_user
from marshmallow import fields, validate

from app.api.schema import (
    AutoMultilangStringField,
    PageSearchParameters,
    PaginatedResponseSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.models.activity import Activity
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import activity_service
from app.service.activity_service import ActivityApiData


class ActivitySchema(RestSchema):
    id = fields.Int(dump_only=True)
    modified = fields.DateTime(dump_only=True)
    name = AutoMultilangStringField(required=True, validate=validate.Length(max=256))
    description = AutoMultilangStringField(required=True)
    start_time = fields.DateTime(required=True)
    end_time = fields.DateTime(required=True)
    location = fields.Str(required=True, validate=validate.Length(max=1024))
    price = fields.Str(required=True, validate=validate.Length(max=256))
    form_id = fields.Int()
    picture = fields.Boolean(dump_only=True)
    picture_file_id = fields.Int(dump_only=True)
    pretix_event_slug = fields.String(validate=validate.Length(max=64))


class ActivityResource(MethodView):
    """Resource to retrieve activity, focused on Pretix based activities."""

    schema = ActivitySchema()

    @require_oauth(Scopes.activity)
    def get(self, activity: Activity):
        return self.schema.dump(activity)

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    @json_schema(schema)
    def put(self, data: ActivityApiData, activity: Activity):
        activity_service.edit_activity_from_pretix(activity, data)
        return Response(status=HTTPStatus.NO_CONTENT)


class ActivityListResource(MethodView):
    schema = ActivitySchema()
    schema_get = PaginatedResponseSchema(ActivitySchema(many=True))

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    @query_parameter_schema(PageSearchParameters)
    def get(self, pagination: PageSearchParameters):
        return self.schema_get.dump(
            activity_service.paginated_search_all_activities(pagination)
        )

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    @json_schema(schema)
    def post(self, data: ActivityApiData):
        activity = activity_service.create_pretix_activity_from_pretix(
            data, current_user
        )
        return self.schema.dump(activity)


class ActivityPictureResource(MethodView):
    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    def put(self, activity: Activity):
        if "file" not in request.files:
            return Response(status=HTTPStatus.BAD_REQUEST)

        activity_service.set_activity_picture(activity, request.files["file"])
        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.activity)
    @require_role(Roles.ACTIVITY_WRITE)
    def delete(self, activity: Activity):
        activity_service.remove_activity_picture(activity)
        return Response(status=HTTPStatus.NO_CONTENT)
