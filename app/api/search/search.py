import pydantic
from flask.views import MethodView
from flask_login import current_user
from marshmallow import Schema, fields

from app.api.schema import RenderedMarkdownStringField
from app.decorators import query_parameter_schema
from app.service.search_service import execute_search_request, get_allowed_indices
from app.utils.markdown_summary import SummaryOptions


class SearchSchema(pydantic.BaseModel):
    query: str
    index: str = "viaduct"
    limit: int = 10
    offset: int = 0


class SearchSourceSchema(Schema):
    group_id = fields.Integer()
    needs_paid = fields.Integer()
    url = fields.URL(relative=True)
    permission = fields.String()
    nl_content = RenderedMarkdownStringField(SummaryOptions(450, "paragraph"))
    en_content = RenderedMarkdownStringField(SummaryOptions(450, "paragraph"))
    nl_title = fields.String()
    nl_sub_title = fields.String()
    en_title = fields.String()
    en_sub_title = fields.String()
    img_url = fields.URL(relative=True)
    index = fields.String()


class SearchHitSchema(Schema):
    _id = fields.String()
    _score = fields.Integer()
    _source = fields.Nested(SearchSourceSchema)


class SearchHitsSchema(Schema):
    total = fields.Integer()
    hits = fields.Nested(SearchHitSchema, many=True)


class SearchResultSchema(Schema):
    took = fields.Integer()
    timed_out = fields.Boolean()
    hits = fields.Nested(SearchHitsSchema)


class SearchResource(MethodView):
    result_schema = SearchResultSchema()

    @query_parameter_schema(SearchSchema)
    def get(self, query: SearchSchema):
        data = execute_search_request(
            user=current_user,
            query=query.query,
            index=query.index,
            limit=query.limit,
            offset=query.offset,
        )

        return self.result_schema.dump(data)


class SearchIndexResource(MethodView):
    def get(self):
        return {"indices": get_allowed_indices(current_user)}
