from http import HTTPStatus

from flask import Response
from flask.views import MethodView
from marshmallow import fields, validate

from app.api.schema import RestSchema
from app.api.user.user import UserSchema
from app.decorators import json_schema, require_oauth
from app.exceptions.base import ResourceNotFoundException
from app.oauth_scopes import Scopes
from app.service import tutoring_service


class TutoringSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    num_hours = fields.Integer(validate=validate.Range(min=0))
    course_id = fields.Integer()
    notes = fields.String()
    tutor = fields.Nested(
        UserSchema(only=("first_name", "last_name", "email", "phone_nr"))
    )
    tutee = fields.Nested(
        UserSchema(only=("first_name", "last_name", "email", "phone_nr"))
    )
    created = fields.DateTime(dump_only=True)


class UserTutoringListResource(MethodView):
    schema = TutoringSchema()

    @require_oauth(Scopes.tutoring)
    def get(self, user):
        return self.schema.dump(
            tutoring_service.get_tutorings_for_tutee(user), many=True
        )

    @require_oauth(Scopes.tutoring)
    @json_schema(schema)
    def post(self, tutoring_data, user):
        t = tutoring_service.create_tutoring(user, **tutoring_data)
        return self.schema.dump(t)


class UserTutoringResource(MethodView):
    schema_get = TutoringSchema()
    schema_patch = TutoringSchema(partial=True)

    @require_oauth(Scopes.tutoring)
    def get(self, user, tutoring):
        # TODO add support for admins to access all users.
        if tutoring.tutee is not user:
            raise ResourceNotFoundException("tutoring", tutoring.id)
        return self.schema_get.dump(tutoring)

    @require_oauth(Scopes.tutoring)
    @json_schema(schema_patch)
    def patch(self, tutoring_data, user, tutoring):
        # TODO add support for admins to access all users.
        if tutoring.tutee is not user:
            raise ResourceNotFoundException("tutoring", tutoring.id)
        tutoring_service.update_tutoring(tutoring, **tutoring_data)
        return self.schema_get.dump(tutoring)

    @require_oauth(Scopes.tutoring)
    def delete(self, user, tutoring):
        # TODO add support for admins to access all users.
        if tutoring.tutee is not user:
            raise ResourceNotFoundException("tutoring", tutoring.id)
        tutoring_service.delete_tutoring(tutoring)
        return Response(status=HTTPStatus.NO_CONTENT)
