import { defineConfig } from "vite";
import { createVuePlugin } from "vite-plugin-vue2";
import { createI18nPlugin } from "@yfwz100/vite-plugin-vue2-i18n";

export default defineConfig({
    publicDir: false,
    build: {
        // generate manifest.json in outDir
        manifest: true,
        rollupOptions: {
            // overwrite default .html entry
            input: ["./frontend/index.ts", "./frontend/assets/scss/app.scss"],
        },
    },
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
            },
            sass: {
                indentedSyntax: true,
            },
        },
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.esm.js",
            "ant-design-vue/types/form/form": "node_modules/ant-design-vue/types/form/form",
            "ant-design-vue/types/transfer": "node_modules/ant-design-vue/types/transfer",
            "ant-design-vue/types/table/column": "node_modules/ant-design-vue/types/table/column",
            "ant-design-vue/types/select/option": "node_modules/ant-design-vue/types/select/option",
        },
    },
    plugins: [createVuePlugin(), createI18nPlugin()],
    server: {
        origin: process.env.VITE_SERVER || "http://localhost:5173",
    },
});
