"""Create tutors table.

Revision ID: 5e52f248e03e
Revises: 40fa75cbc518
Create Date: 2020-09-15 21:24:27.847317

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "5e52f248e03e"
down_revision = "40fa75cbc518"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "tutor",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created", sa.DateTime(timezone=True), nullable=True),
        sa.Column("modified", sa.DateTime(timezone=True), nullable=True),
        sa.Column("user_id", sa.Integer(), nullable=False),
        sa.Column("course_id", sa.Integer(), nullable=False),
        sa.Column("num_hours", sa.Integer(), nullable=False),
        sa.Column("approved", sa.DateTime(timezone=True)),
        sa.Column("grade", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ("course_id",), ["course.id"], name=op.f("fk_tutor_course_id_course")
        ),
        sa.ForeignKeyConstraint(
            ("user_id",), ["user.id"], name=op.f("fk_tutor_user_id_user")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_tutor")),
        sqlite_autoincrement=True,
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("tutor")
    # ### end Alembic commands ###


# vim: ft=python
