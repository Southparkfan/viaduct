"""Add DataNose code to education.

Revision ID: 91b9c53dab53
Revises: eb2f1e48c386
Create Date: 2019-02-09 14:06:36.745367

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "91b9c53dab53"
down_revision = "eb2f1e48c386"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "education", sa.Column("datanose_code", sa.String(length=64), nullable=True)
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("education", "datanose_code")
    # ### end Alembic commands ###


# vim: ft=python
