"""empty message.

Revision ID: 8f9031f196eb
Revises: ('ceb39ad3f5c5', '4db52787ccf6')
Create Date: 2019-05-06 14:23:51.713543

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "8f9031f196eb"
down_revision = ("ceb39ad3f5c5", "4db52787ccf6")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()


def downgrade():
    create_session()


# vim: ft=python
