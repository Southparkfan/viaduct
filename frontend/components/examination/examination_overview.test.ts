import { createLocalVue } from "@vue/test-utils";
import { fireEvent, render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import ExaminationOverview from "./examination_overview.vue";
import { courseApi } from "../../scripts/api/course";
import flushPromises from "flush-promises";
import { examinationApi } from "../../scripts/api/examination";
import Flask from "../../utils/flask";
import VueRouter from "vue-router";
import { i18n } from "../../translations";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);

jest.mock("../../scripts/api/course", () => {
    return {
        courseApi: {
            getCourses: jest.fn(() =>
                Promise.resolve({
                    data: {
                        page: 1,
                        page_size: 10,
                        page_count: 1,
                        total: 2,
                        data: [
                            {
                                id: 74,
                                name: "Advanced Networking",
                                datanose_code: "5384ADNE6Y",
                            },
                        ],
                    },
                })
            ),
        },
    };
});
jest.mock("../../scripts/api/examination", () => {
    return {
        examinationApi: {
            getExamsByCourse: jest.fn(() =>
                Promise.resolve({
                    data: [
                        {
                            id: 1,
                            comment: null,
                            date: "2016-05-24",
                            examination_file_id: 1,
                            answers_file_id: 2,
                            course: {
                                id: 74,
                                name: "Advanced Networking",
                                datanose_code: "5384ADNE6Y",
                            },
                            test_type: "Unknown",
                        },
                    ],
                })
            ),
        },
    };
});

jest.mock("../../utils/flask");
const consoleSpy = jest.spyOn(console, "error");

describe(ExaminationOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly and expands", async () => {
        const { getByText } = render(ExaminationOverview, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });

        expect(courseApi.getCourses).toBeCalledWith("", 1, [], []);
        getByText("Examinations");
        await flushPromises();
        const row = getByText("Advanced Networking");
        await fireEvent.click(row);
        await flushPromises();
        expect(examinationApi.getExamsByCourse).toBeCalledWith(74);
        getByText("2016-05-24");
        expect(Flask.url_for).toBeCalledWith("login.sign_in");
        expect(Flask.url_for).toBeCalledWith("examination.view", {
            exam: 1,
            doc_type: "exam",
        });
        expect(Flask.url_for).toBeCalledWith("examination.view", {
            exam: 1,
            doc_type: "answers",
        });
    });
});
