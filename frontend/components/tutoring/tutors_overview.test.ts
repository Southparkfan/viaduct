import { createLocalVue } from "@vue/test-utils";
import { render } from "@testing-library/vue";
import VueI18n from "vue-i18n";
import TutorOverview from "./tutors_overview.vue";
import { i18n } from "../../translations";
import VueRouter from "vue-router";

jest.mock("../../utils/flask.ts");
jest.mock("../../scripts/api/page.ts");
jest.mock("../../scripts/api/tutoring.ts");
const localVue = createLocalVue();
localVue.use(VueI18n);
localVue.use(VueRouter);

const consoleSpy = jest.spyOn(console, "error");

describe(TutorOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutorOverview, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });

        getByText("Tutor overview");
    });
});
