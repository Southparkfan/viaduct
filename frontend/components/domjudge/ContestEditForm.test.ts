import { render } from "@testing-library/vue";
import VueRouter from "vue-router";
import ContestEditForm from "./ContestEditForm.vue";
import { i18n } from "../../translations";
import { createLocalVue } from "@vue/test-utils";
import VueI18n from "vue-i18n";

jest.mock("../../utils/flask.ts");
const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(VueI18n);

const consoleSpy = jest.spyOn(console, "error");

describe(ContestEditForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(ContestEditForm, {
            localVue: localVue,
            router: new VueRouter(),
            i18n: i18n,
        });

        getByText("DOMjudge");
    });
});
