export default function debounce(fn, delay) {
    let timeout: number;
    return function (...args) {
        if (timeout !== undefined) {
            clearTimeout(timeout);
        }

        timeout = window.setTimeout(() => fn.apply(this, args), delay);
    };
}
