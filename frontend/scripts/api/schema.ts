import { Api } from "./api";
import Flask from "../../utils/flask";
import { JsonSchema } from "../../types/schema";

class SchemaApi extends Api {
    getSchema(s: string) {
        return this.get<JsonSchema>(
            Flask.url_for("api.schema", { schema_name: s })
        );
    }
}

export const schemaApi = new SchemaApi();
