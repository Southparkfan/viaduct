import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";

export class RankingUser {
    first_name: string;
    last_name: string;
    id: number;
}

export class RankingResponse {
    points: number;
    user: RankingUser;
}

export type ChallengeResponse = {
    id: number;
    name: string;
    description: string;
    start_date: string;
    end_date: string;
    hint: string;
    type: string;
    open: boolean;
    weight: number;
    approved: boolean;
    answer: string;
};

export type ChallengeRequest = Omit<ChallengeResponse, "Id">;

export class SubmissionResponse {
    approved: boolean;
    validated: boolean;
}

class ChallengeApi extends Api {
    getChallenge(challengeId: number): AxiosPromise<ChallengeResponse> {
        return this.get<ChallengeResponse>(
            Flask.url_for("api.challenge", { challenge: challengeId })
        );
    }

    createChallenge(values: ChallengeRequest): AxiosPromise<ChallengeResponse> {
        return this.post<ChallengeResponse>(
            Flask.url_for("api.challenges"),
            values
        );
    }

    updateChallenge(
        challengeId: number,
        values: ChallengeRequest
    ): AxiosPromise<ChallengeResponse> {
        return this.put<ChallengeResponse>(
            Flask.url_for("api.challenge", { challenge: challengeId }),
            values
        );
    }

    deleteChallenge(challengeId: number): AxiosPromise<void> {
        return this.delete<void>(
            Flask.url_for("api.challenge", { challenge: challengeId })
        );
    }

    getRanking(): AxiosPromise<RankingResponse[]> {
        return this.get<RankingResponse[]>("/api/challenges/ranking/");
    }

    getChallengesForUser(): AxiosPromise<ChallengeResponse[]> {
        return this.get<ChallengeResponse[]>("/api/challenges/users/self/");
    }

    async submitChallenge(challengeId: number, submission: string) {
        return this.post<SubmissionResponse>(
            Flask.url_for("api.challenges.submission", {
                challenge: challengeId,
            }),
            { submission: submission }
        );
    }

    async submitChallengeAdmin(challengeId: number, userId: number) {
        return this.post<unknown>(
            Flask.url_for("api.challenge_submission_admin", {
                challenge: challengeId,
            }),
            { user_id: userId }
        );
    }

    async clearRanking() {
        return this.delete<unknown>(Flask.url_for("api.challenges.ranking"));
    }
}

export const challengeApi = new ChallengeApi();
